<?php
ini_set ("display_errors", "1");
error_reporting(E_ALL);
#print_r($argv);
if(1>=count($argv)){
	print "usage: portal_grid_metadata.php Database_Name Table_Name \n";
	exit;
}

$link = mysqli_connect("localhost", "gadb", "noidontthinkso",$argv[1]) or die("Could not connect: ".mysql_error());

if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$result = mysqli_query($link,sprintf("DESCRIBE %s;",$argv[2]));

$table_fields = array();
while($row = mysqli_fetch_array($result)) {
	$table_fields[trim($row[0])] = $row;
}
#print_r($table_fields);

$colFile = sprintf("inc_%s.js",$argv[2]);
$fh = fopen($colFile, 'w') or die("can't open file");
$stringData = sprintf("var get%sColumns = function(){\n\tvar columns=[\n\t\t{\n",$argv[2]);
foreach($table_fields as $k => $tf) {
	$file_fields[] = sprintf("\t\t\ttext:'%s',\n\t\t\tname:'%s',\n\t\t\tdataIndex:'%s',\n\t\t\twidth:%d,\n\t\t\tsortable:true,\n\t\t\ttype:'string',\n\t\t\tuseNull:false\n",$k,$k,$k,(strlen($k)*5)+80);

}
$stringData .= implode("\t\t},{\n",$file_fields)."\t\t},\n\t];\n\treturn columns;\n};\n";
fwrite($fh, $stringData);
print $stringData;
fclose($fh);

?>
