<?php
    require_once('lib/mysql_gadb.inc.php'); # get $mysqli

    function isInteger($i){ return ($i == (string)(int)$i);}

    function isFloat($f){ return ($f == (string)(float)$f);}

    ini_set ("display_errors", "1");
    error_reporting(E_ALL);
    #print_r($argv);
    if(1>=count($argv)){
            print "usage: gadb_import.php Metadata_File_Name [Alternate_DB_Name] [Alternate_Table_Name]\n";
            exit;
    }

    $file_name = (isset($argv[1]))?$argv[1]:0;
    $db_name = (isset($argv[2]))?$argv[2]:"gadb-dev";
    $table_name = (isset($argv[3]))?$argv[3]:"test_files";

    $mysqli->select_db($db_name) or die("Could not connect: ".mysqli_error());

    if (mysqli_connect_errno()) {
      echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

    if($result = $mysqli->query(sprintf("SHOW TABLES LIKE '%s';",$table_name)) OR DIE ($mysqli->error)){
        if($result->num_rows>0){
            printf("Table '%s' found, exporting to %s_%s_old.tsv",$table_name,$db_name,$table_name);
            $table_dump = array();
            $result = $mysqli->query(sprintf("SELECT * FROM %s;",$table_name)) OR DIE ($mysqli->error);
            while($row = $result->fetch_array(MYSQLI_ASSOC)){
                $table_dump[] = array_slice($row,1);
            }

            echo "\nWriting old table to output file.";
            $fp = fopen(sprintf("%s_%s_old.tsv",$db_name,$table_name), "w");
            fwrite($fp, sprintf("%s\r\n",implode("\t",array_keys($table_dump[0]))));
            foreach($table_dump as $i => $t_d_row){
                fwrite($fp, sprintf("%s\r\n",implode("\t",$t_d_row)));
                echo (($i%100)==0)?".":"";
            }
            fclose($fp);
            printf("\n%s_%s_old.tsv successfully created; Dropping table '%s'.",$db_name,$table_name,$table_name);
            if($result = $mysqli->query(sprintf("DROP TABLE IF EXISTS `%s`;",$table_name))){
                printf("\nTable '%s' Successfully Dropped.",$table_name);
            }else{
                printf("\nAn error occured when trying to drop table '%s'. Exiting.",$table_name);
                exit;
            }
        }else{
            printf("\nTable '%s' not found, moving on.",$table_name);
        }
    }

    $metadata = array();
    $add_fields = array();
    printf("\nReading contents of metadata file '%s'.",$table_name);
    if($lines = file($file_name)){
        $file_fields = explode("\t",trim(array_shift($lines)));
        foreach($lines as $i => $l){
                $metadata[] = explode("\t",rtrim($l,"\n"));
                echo (($i%100)==0)?".":"";
        }

        foreach($file_fields as $k => $ff){
            if(empty($metadata[0][$k])){
                    $add_fields[] = sprintf("`%s` varchar(255)",$ff);
            }
            elseif(isInteger($metadata[0][$k])){
                    $add_fields[] = sprintf("`%s` INT(10)",$ff);
            }
            elseif(isFloat($metadata[0][$k])){
                    $add_fields[] = sprintf("`%s` FLOAT(10)",$ff);
            }
            else{
                    $add_fields[] = sprintf("`%s` varchar(255)",$ff);
            }
        }

        $table_query = sprintf("CREATE TABLE IF NOT EXISTS `%s` (`FID` int(10) unsigned NOT NULL AUTO_INCREMENT, %s, PRIMARY KEY (`FID`));",
            $table_name,implode(" DEFAULT NULL,",$add_fields)); 
        if($result = $mysqli->query($table_query) OR DIE ($mysqli->error)){
            printf("\nTable '%s' successfully created.",$table_name);
        }

        printf("\nInserting metadata from file '%s' into table '%s'.",$file_name,$table_name);

        $inserts = array();
        foreach($metadata as $i => $md_row){
                $md_row_escaped = array();
                foreach($md_row as $unescaped){
                        $md_row_escaped[] = $mysqli->real_escape_string($unescaped);
                }
                $inserts[] = sprintf("('%s')",implode("','",$md_row_escaped));
                echo (($i%100)==0)?".":"";
        }

        $query_fields = array();
        foreach($file_fields as $ff){
                $query_fields[] = sprintf("`%s`",$ff);
        }

        #print_r(implode(",",$query_fields));

        if($result = $mysqli->query(sprintf("INSERT INTO `%s` (%s) VALUES %s;",$table_name,implode(",\n",$query_fields),implode(",\n",$inserts))) OR DIE ($mysqli->error)){
            echo "\nAll inserts successful. Import script completed normally.";
        }
    }else{
        printf("Unable to read metadata file '%s'.",$file_name);
    }

    exit;

?>
