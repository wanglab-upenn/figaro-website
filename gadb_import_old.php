<?php

function isInteger($i){ return ($i == (string)(int)$i);}

function isFloat($f){ return ($f == (string)(float)$f);}

ini_set ("display_errors", "1");
error_reporting(E_ALL);
#print_r($argv);
if(1>=count($argv)){
	print "usage: import.php Database_Name Table_Name File_Name [FORCE]\n";
	exit;
}

$link = mysqli_connect("localhost", "gadb", "noidontthinkso",$argv[1]) or die("Could not connect: ".mysqli_error());

if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$result = mysqli_query($link,sprintf("DESCRIBE %s;",$argv[2]));

$table_field_names = array();
while($row = mysqli_fetch_array($result)) {
	$table_fields[trim($row[0])] = $row;
}

$missing_fields = array();
$metadata = array();
$lines = file($argv[3]);
$file_fields = explode("\t",trim(array_shift($lines)));
#print_r($table_fields);
#print_r($file_fields);
#print_r($lines[0]);

foreach($lines as $l){
	$metadata[] = explode("\t",rtrim($l,"\n"));
}
#print_r($metadata[0]);

foreach($file_fields as $f){
	if(!in_array($f,array_keys($table_fields))){
		$missing_fields[] = $f;
	}
}
#print_r($missing_fields);

if(!empty($missing_fields)){
	if($argv[4]=="FORCE"){
		/*
		foreach($table_fields as $tf_name => $tf_atr){
			$add_fields[] = sprintf("`%s` %s%s%s",
				$tf_name,
				$tf_atr[1],
				($tf_atr[2]=="NO")?" NOT NULL":"",
				($tf_atr[3]=="PRI")?" PRIMARY KEY":"");
		}
		*/
		#print_r($missing_fields);

		foreach($file_fields as $k => $ff){
			#print "[".$missing_fields[$k]."]\n";
			#print "[".$metadata[0][$k+count($table_fields)]."]\n";
			if(in_array($ff,$missing_fields)){
				if(empty($metadata[0][$k])){
					$add_fields[] = sprintf("ADD COLUMN `%s` varchar(64)",$ff);
				}
				elseif(isInteger($metadata[0][$k])){
					$add_fields[] = sprintf("ADD COLUMN `%s` INT(10)",$ff);
				}
				elseif(isFloat($metadata[0][$k])){
					$add_fields[] = sprintf("ADD COLUMN `%s` FLOAT(10)",$ff);
				}
				else{
					$add_fields[] = sprintf("ADD COLUMN `%s` varchar(64)",$ff);
				}
			}
		}
		#print_r($add_fields);

		$table_query = sprintf("ALTER TABLE `%s` %s;",$argv[2],implode(",",$add_fields)); 
		print $table_query."\n";
		#print($table_query);
		$result = mysqli_query($link,$table_query) or die("Unable to alter table: ".mysqli_error());
	}
	else{
		printf("The following (file) fields are missing from the %s_attribute table:\n%s\n", $argv[2], implode("\n",$missing_fields));
		exit;
	}
}

$inserts = array();
foreach($metadata as $md_row){
	$md_row_escaped = array();
	$query_fields = array();
	foreach($md_row as $i => $unescaped){
		$md_row_escaped[] = mysqli_real_escape_string($link,$unescaped);
	}
	#print_r($md_row_escaped);
	$inserts[] = sprintf("('%s')",implode("','",$md_row_escaped));
/*
	foreach($file_fields as $j=>$ff){
		if($j!=1){
			$query_fields[] = sprintf("`%s`='%s'",$ff,$md_row_escaped[$j]);
		}
	}
	$update_query = sprintf("UPDATE `%s`.`%s` SET %s WHERE `%s` = '%s';",$argv[1],$argv[2],implode(",\n",$query_fields),$file_fields[1],$md_row_escaped[1]); 
	print $update_query."\n";
	$result = mysqli_query($link,$update_query) or die("Unable to insert rows: ".mysqli_error($link));
	sleep(.25);
*/
}
#print_r($inserts);
#print_r($file_fields);
$query_fields = array();
foreach($file_fields as $ff){
	$query_fields[] = sprintf("`%s`",$ff);
}

$insert_query = sprintf("INSERT INTO `%s` (%s) VALUES %s;",$argv[2],implode(",\n",$query_fields),implode(",\n",$inserts)); 
//print($insert_query);
$result = mysqli_query($link,$insert_query) or die("Unable to insert rows: ".mysqli_error($link));

?>
