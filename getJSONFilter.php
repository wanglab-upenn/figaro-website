<?php

    require_once('lib/mysql_gadb.inc.php'); # get $mysqli

if($argv[1]==="MAKE"){
    $query = "SELECT `COLUMN_NAME` 
	FROM `INFORMATION_SCHEMA`.`COLUMNS` 
	WHERE `TABLE_SCHEMA`='gadb' 
    	AND `TABLE_NAME`='files';";

#    file_put_contents("sql_fltr_str.txt", print_r($sqlStr,true));

    if ($result = $mysqli->query($query) or die ($mysqli->error) ) {
        $tot= $result->num_rows;
        while ($row = $result->fetch_array(MYSQLI_ASSOC)){
             $res[] = $row;
        }
    }

	if($res){
		foreach($res as $cols){
			#printf("SELECT '%s' AS `Key`,COUNT(DISTINCT `%s`) AS vCount, GROUP_CONCAT(DISTINCT `%s` SEPARATOR ',') AS `Values` FROM `files`; \n",$cols["COLUMN_NAME"],$cols["COLUMN_NAME"],$cols["COLUMN_NAME"]);
			$query = sprintf("SELECT '%s' AS `fKey`,COUNT(DISTINCT `%s`) AS vCount, GROUP_CONCAT(DISTINCT `%s` SEPARATOR ',') AS `fValues` FROM `files`;",
			$cols["COLUMN_NAME"],
			$cols["COLUMN_NAME"],
			$cols["COLUMN_NAME"]);
				if ($result = $mysqli->query($query) or die ($mysqli->error) ) {
				while ($row = $result->fetch_array(MYSQLI_ASSOC)){
				if($row["vCount"]>1&&$row["vCount"]<200){
					#print_r($row);
							$usefulFilters[] = $row;
				}
					}
			}
		}
		foreach($usefulFilters as $uf){
			$query = sprintf("INSERT INTO `Filters` (`fKey`,`vCount`,`fValues`) VALUES ('%s',%d,'%s');",
			$uf["fKey"],
			$uf["vCount"],
			$mysqli->real_escape_string($uf["fValues"]));
				if ($result = $mysqli->query($query) or die ($mysqli->error) ) {
			#OK~!
			}
		}
	}    
}elseif(isset($argv[1])){
    //pull from table
	$query = ($argv[1]==="root")
		?"SELECT `fKey` FROM `Filters` WHERE `fType` != 0;"
		:sprintf("SELECT `fValues` FROM `Filters` WHERE `fKey` = '%s';",
			$argv[1]);
	
    if ($result = $mysqli->query($query) or die ($mysqli->error) ) {
		if(mysqli_num_rows($result)!==0){
			while ($row = $result->fetch_array(MYSQLI_ASSOC)){
				if($argv[1]=="root"){
					$tableFilters[$argv[1]][] = $row["fKey"];
				}else{
					foreach(explode(",",$row["fValues"]) as $h){$tableFilters[$argv[1]][] = $h;}
				}
			}
		}else{
			$query = "SELECT `fKey` FROM `Filters` WHERE `fType` != 0;";
			$result = $mysqli->query($query) or die ($mysqli->error); 
			$_REQUEST["node"]="root2";
			while ($row = $result->fetch_array(MYSQLI_ASSOC)){
				$tableFilters["root"][] = $row["fKey"];
			}
		}
    }

    //$tot = count($tableFilters);
	foreach ($tableFilters as $k => $v){
			foreach($v as $var){
			$out["text"] = $var;
			$out["id"] = $var;
			$out["expanded"] = false;
			$out["leaf"] = ($argv[1]=="root")?false:false;
			($argv[1]!="root")?$out["handler"] = "function(){alert('".$argv[1]."');}":0;
			$bigout[] = $out;
		}
	}

    // Print out json for ExtJS
    //$jStr = json_encode($tableFilters);
    $jStr = json_encode($bigout);
    print '{"text":"'.$argv[1].'","id":"'.$argv[1].'","children":' . $jStr . '}';

}else{
	$extra ="";
    //pull from table
	$testtt = explode(":",$_REQUEST["node"]);
	$query = ($_REQUEST["node"]==="root")
		?"SELECT `fKey` FROM `Filters` WHERE `fType` != 0;"
		:sprintf("SELECT `fValues` FROM `Filters` WHERE `fKey` = '%s';",
			($testtt[1]!=="nonroot")?$_REQUEST["node"]:$testtt[0]);
	
    if ($result = $mysqli->query($query) or die ($mysqli->error) ) {
		if(mysqli_num_rows($result)!==0){
			while ($row = $result->fetch_array(MYSQLI_ASSOC)){
				if($_REQUEST["node"]=="root"){
					$tableFilters[$_REQUEST["node"]][] = $row["fKey"];
				}else{
					foreach(explode(",",$row["fValues"]) as $h){$tableFilters[$_REQUEST["node"]][] = $h;}
				}
			}
		}else{
			$query = "SELECT `fKey` FROM `Filters` WHERE `fType` != 0;";
			$result = $mysqli->query($query) or die ($mysqli->error); 
			//$_REQUEST["node"]="root2";
			$extra=":nonroot";
			while ($row = $result->fetch_array(MYSQLI_ASSOC)){
				$tableFilters[$_REQUEST["node"]][] = $row["fKey"];
			}
		}
    }

    //$tot = count($tableFilters);
	foreach ($tableFilters as $k => $v){
			foreach($v as $var){
			$out["text"] = $var;
			$out["id"] = $var.$extra;
			$out["expanded"] = false;
			$out["leaf"] = ($_REQUEST["node"]==="root")?false:true;
			//$out["leaf"] = ($testtt[1]=="nonroot")?true:false;
			($_REQUEST["node"]!="root")?$out["handler"] = "function(){alert('".$_REQUEST["node"]."');}":0;
			$bigout[] = $out;
		}
	}

    // Print out json for ExtJS
    //$jStr = json_encode($tableFilters);
    //print '{"totalCount":"' . $tot . '","data":' . $jStr . '}';
    $jStr = json_encode($bigout);
    print '{"text":"'.$_REQUEST["node"].'","id":"'.$_REQUEST["node"].'","children":' . $jStr . '}';
}
     
    $mysqli->close();
    
?>
