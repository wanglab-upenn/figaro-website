<?php
    $sourceURL = $_SERVER["HTTP_HOST"].$_SERVER["SCRIPT_NAME"];
    require_once('lib/mysql_gadb.inc.php'); # get $mysqli
    define('DEBUG_ME',1);

    $rowCount= (empty($_REQUEST['limit']) ? 50 : $_REQUEST['limit']);
    $offset= (empty($_REQUEST['start']) ? 0 : $_REQUEST['start']);
    
    $whereClause = "WHERE 1 ";
    $orderClause="ORDER BY `Identifier`";

           #print_r($_REQUEST);
    if(isset($_REQUEST['sort'])){
        $submittedSorts = json_decode($_REQUEST['sort'],true);
        #print_r($submittedSorts);
        
        foreach ($submittedSorts as &$prop){
              if(isset($prop['property'])){
                if (!isset($myFilters[ $prop['property'] ])){
                  $mySorts["`".$prop['property']."`"] = array();
                }
                
                $arr = $mySorts["`".$prop['property']."`"];
                $arr[] =  $prop['direction'] ;
                
                $mySorts["`".$prop['property']."`"] = $arr;
              }
           }
           
           (DEBUG_ME == 1)?file_put_contents("sort_query_str.txt",print_r($mySorts,true)):0;
           
           if ( count($mySorts) >0 ){
              $ct=0;
              $orderClause = "ORDER BY ";
              foreach ($mySorts as $k => $v){
                if ($ct>0){$orderClause .= ", ";}
                $orderClause .= sprintf("%s %s",$k,join(",", array_values($mySorts[$k])));
                $ct++;
              }
              (DEBUG_ME == 1)?file_put_contents("sort_str.txt",print_r($orderClause,true)):0;
           }
            #print_r($mySorts);
    }
    
    if(isset($_REQUEST['filter'])){
           $submittedFilters = json_decode($_REQUEST['filter'],true);
           
           (DEBUG_ME == 1)?file_put_contents("getjsonq-isubmitted.txt",print_r($submittedFilters,true)):0;
           
           foreach ($submittedFilters as &$prop){
              if(isset($prop['property'])){
                if (!isset($myFilters[ $prop['property'] ])){
                  $myFilters[ $prop['property'] ] = array();
                }
                $arr = $myFilters[ $prop['property'] ];
				if (is_array( $prop['value'])){
                   if (array_key_exists('Min', $prop['value'] )){
                     $arr['Min']= sprintf(" >= %d",$prop['value']['Min']);
                   }
                   
                   if (array_key_exists('Max', $prop['value'] )){
                     $arr['Max']= sprintf(" <= %d",$prop['value']['Max']);
                   }
                }else{
                   $arr["values"][] = $mysqli->real_escape_string($prop['value']);                
                }
              	if(isset($prop['type'])){
					$arr["type"] = $prop['type'];
				}
              	if(isset($prop['exact'])){
					$arr["exact"] = $prop['exact'];
				}
              	if(isset($prop['exclude'])){
					$arr["exclude"] = $prop['exclude'];
				}
              	if(isset($prop['externalFile'])){
					$arr["externalFile"] = $prop['externalFile'];
				}
              	if(isset($prop['externalField'])){
					$arr["externalField"] = $prop['externalField'];
				}
                $myFilters[ $prop['property'] ] = $arr;
              }
           }

           (DEBUG_ME == 1)?file_put_contents("getjsonq-parsed.txt",print_r($myFilters,true)):0;

		  //IF EXTERNAL FILE IS SPECIFIFED
		  foreach($myfilters as $k => $v){
			
		  }	
           
           if ( count($myFilters) >0 ){
              $ct=1;
              foreach ($myFilters as $k => $v){
                if(array_key_exists('type',$myFilters[$k])){
					if(!empty($myFilters[$k]["values"][0])){
                		($ct>0)?$whereClause .= " AND ":1;
                  		$whereClause .= sprintf("`%s` %s LIKE '%s%s%s'",$k,($myFilters[$k]["exclude"]==1)?"NOT":"",($myFilters[$k]["exact"]!=1)?"%":"",$myFilters[$k]["values"][0],($myFilters[$k]["exact"]!=1)?"%":"");
					}
                }elseif (array_key_exists('externalFile', $myFilters[$k]) && array_key_exists('externalField', $myFilters[$k])){
					$eData = (array)json_decode(file_get_contents($myFilters[$k]["externalFile"]));
					foreach($eData['tracks'] as $track){
						$fName=$track->$myFilters[$k]["externalField"];
						if(strpos($fName,'.bed')!==false){
							$ayytest[]=sprintf("'%s'",substr($fName,0,strpos($fName,'.bed')));
						}
					}
                	($ct>0)?$whereClause .= " AND ":1;
                  	$whereClause .= "`" . $k . "`" . " IN ";
                  	$whereClause .= "(" . join(",", $ayytest) . ")";
				}elseif (array_key_exists('Min', $myFilters[$k]) ||
                    array_key_exists('Max', $myFilters[$k])
                    ){
                	($ct>0)?$whereClause .= " AND ":1;
                  $rangeCt=0;
                  foreach ($myFilters[$k] as $rangeKey => $rangeOp){
                    ($rangeCt>0)? $whereClause .= " AND ":1;
                    $whereClause .= "`" . $k . "`" ;
                    $whereClause .= $rangeOp;
                    $rangeCt++;
                  }
                }else{
					$encValues=array();
					foreach ($myFilters[$k]["values"] as $val){
						$encValues[]=sprintf("'%s'",$val);
					}
                	($ct>0)?$whereClause .= " AND ":1;
                  	$whereClause .= "`" . $k . "`" . " NOT IN ";
                  	$whereClause .= "(" . join(",", $encValues) . ")";
                }
                $ct++;
              }
           }
			file_put_contents("testout.txt",print_r($myFilters,true));
           
           (DEBUG_ME == 1)?file_put_contents("getjsonq-where_sql.txt",print_r($whereClause,true)):0;
    }

if((isset($_REQUEST["action"]) && $_REQUEST["action"]==="export") || $argv[2]==="export"){
	if(isset($_REQUEST["arrayIDs"]) || $argv[3]==="arrayIDs"){
	$listIDs = $_REQUEST["arrayIDs"];
	$whereClause = sprintf("WHERE `%s` IN(%s)",'CELLID',$listIDs);
	}
}
    
if(isset($_REQUEST["fsids"])){
    $query = sprintf("SELECT `MID`,`Save_Type` FROM `Saved_Meta` WHERE `MID` IN (%s) AND `Save_Type` = 1;",mysqli_real_escape_string($mysqli,$_REQUEST["fsids"]));
    if ($result = $mysqli->query($query)){
        if(mysqli_num_rows($result)!==0){
            while ($row = $result->fetch_assoc()){
                $tableSets[] = $row["MID"];
            }
            if(count($tableSets)>0){
                $whereClause = sprintf("WHERE `FID` IN (SELECT `File_ID` FROM `Saved_Sets_Data` LEFT JOIN `Saved_Sets` ON `Saved_Sets`.`Set_ID` = `Saved_Sets_Data`.`SID` WHERE `Meta_ID` IN(%s))",implode(",",$tableSets));
            }else{
                //ZERO ROWS RETURNED
                echo "error";
                exit;
            }
        }
    }
}

    if(isset($_REQUEST["check"]) || $argv[1]=="check"){
        #print_r($mySorts);

        (empty($mySorts))?$mySorts["`FID`"] = array("ASC"):0;
        (!in_array("`FID`",array_keys($mySorts)))?$mySorts["`FID`"] = array("ASC"):0;
        $sqlStr = sprintf("SELECT %s FROM `files` %s %s;",implode(",",array_keys($mySorts)),$whereClause,$orderClause);
        #echo $sqlStr;
        $fp = fopen("./files_sql_str.txt","w");
        fwrite($fp, print_r($sqlStr,true));
        fclose($fp);

        $result = $mysqli->query($sqlStr) OR DIE ($mysqli-error);
        $tot= $result->num_rows;
        while($row = $result->fetch_array(MYSQLI_ASSOC)){
            $res[] = $row;
        }
    }elseif(isset($_REQUEST["fids"])){
        $sqlStr = sprintf("SELECT * FROM `files` WHERE `FID` IN(%s) %s;",$_REQUEST["fids"],$orderClause);
        #echo $sqlStr;
        $result = $mysqli->query($sqlStr) OR DIE ($mysqli->error);
        $tot= $result->num_rows;
        while($row = $result->fetch_array(MYSQLI_ASSOC)){
            $res[] = $row;
        }
    }else{
        $sqlStr = sprintf("SELECT * FROM `files` %s %s;",$whereClause,$orderClause);

        $fp = fopen("./files_sql_str.txt","w");
        fwrite($fp, print_r($sqlStr,true));
        fclose($fp);

        if ($result = $mysqli->query($sqlStr)) {
            $tot= $result->num_rows;
            $result->data_seek($offset);

            $ct=0;

            for ($res = array(); $tmp = $result->fetch_array(MYSQLI_ASSOC);$ct++){
                $res[] = $tmp;

                if ($ct>$rowCount-2){break;}
            }
        }else{
            print $mysqli->error; 
        }
    }
    // Print out json for ExtJS
    $jStr = json_encode($res);
    //$jStr2 = json_encode($fullResults);

if((isset($_REQUEST["action"]) && $_REQUEST["action"]==="export") || $argv[2]==="export"){
    $lines[] = implode(",",array_keys($res[0]));
    foreach($res as $row){
        $lines[] = implode(",",$row);
    }
    print implode("\r\n",$lines); 
}else{
    #print '{"totalCount":"' . $tot . '","data":' . $jStr . ', "fullDataDump":' . $jStr2 . '}';
    print '{"debug":"'.$sqlStr.'","totalCount":"' . $tot . '","data":' . $jStr . '}';
}     
    $mysqli->close();
    
?>
