<?php
    require_once('lib/mysql_gadb.inc.php'); # get $mysqli
    define('DEBUG_ME',1);

    $rowCount= (empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit']);
    $offset= (empty($_REQUEST['start']) ? 0 : $_REQUEST['start']);
    
    $orderClause="ORDER BY `Identifier`";

           #print_r($_REQUEST);
    
    if(isset($_REQUEST['sort'])){
        $submittedSorts = json_decode($_REQUEST['sort'],true);
        
        foreach ($submittedSorts as &$prop){
              if(isset($prop['property'])){
                if (!isset($myFilters[ $prop['property'] ])){
                  $mySorts[ $prop['property'] ] = array();
                }
                
                $arr = $mySorts[ $prop['property'] ];
                $arr[] =  $prop['direction'] ;
                
                $mySorts[ $prop['property'] ] = $arr;
              }
           }
           
           (DEBUG_ME == 1)?file_put_contents("sort_query_str.txt",print_r($mySorts,true)):0;
           
           if ( count($mySorts) >0 ){
              $ct=0;
              $orderClause = "ORDER BY ";
              foreach ($mySorts as $k => $v){
                if ($ct>0){$orderClause .= ", ";}
                $orderClause .= sprintf("`%s` %s",$k,join(",", array_values($mySorts[$k])));
                $ct++;
              }
              (DEBUG_ME == 1)?file_put_contents("sort_str.txt",print_r($orderClause,true)):0;
           }
            #print_r($submittedSorts,true);
    }
    
    $whereClause="WHERE sm.SampleSource IS NOT NULL AND sra_sample_id ";
    $whereClause="WHERE SampleSource IS NOT NULL AND sra_sample_id IS NOT NULL AND  WellADSPQC=1 ";
    $whereClause="";

    if(isset($_REQUEST['filter'])){
           $submittedFilters = json_decode($_REQUEST['filter'],true);
           
           (DEBUG_ME == 1)?file_put_contents("getjsonq-isubmitted.txt",print_r($submittedFilters,true)):0;
           
           foreach ($submittedFilters as &$prop){
              if(isset($prop['property'])){
                if (!isset($myFilters[ $prop['property'] ])){
                  $myFilters[ $prop['property'] ] = array();
                }
                $arr = $myFilters[ $prop['property'] ];
				if (is_array( $prop['value'])){
                   if (array_key_exists('Min', $prop['value'] )){
                     $arr['Min']= sprintf(" >= %d",$prop['value']['Min']);
                   }
                   
                   if (array_key_exists('Max', $prop['value'] )){
                     $arr['Max']= sprintf(" <= %d",$prop['value']['Max']);
                   }
                }else{
                   $arr["values"][] = $mysqli->real_escape_string($prop['value']);                
                }
              	if(isset($prop['type'])){
					$arr["type"] = $prop['type'];
				}
              	if(isset($prop['exact'])){
					$arr["exact"] = $prop['exact'];
				}
              	if(isset($prop['exclude'])){
					$arr["exclude"] = $prop['exclude'];
				}
              	if(isset($prop['externalFile'])){
					$arr["externalFile"] = $prop['externalFile'];
				}
              	if(isset($prop['externalField'])){
					$arr["externalField"] = $prop['externalField'];
				}
                $myFilters[ $prop['property'] ] = $arr;
              }
           }

           (DEBUG_ME == 1)?file_put_contents("getjsonq-parsed.txt",print_r($myFilters,true)):0;

		  //IF EXTERNAL FILE IS SPECIFIFED
		  foreach($myfilters as $k => $v){
			
		  }	
           
           if ( count($myFilters) >0 ){
              $ct=1;
              $whereClause = "WHERE 1 ";
              foreach ($myFilters as $k => $v){
                if(array_key_exists('type',$myFilters[$k])){
					if(!empty($myFilters[$k]["values"][0])){
                		($ct>0)?$whereClause .= " AND ":1;
                  		$whereClause .= sprintf("`%s` %s LIKE '%s%s%s'",$k,($myFilters[$k]["exclude"]==1)?"NOT":"",($myFilters[$k]["exact"]!=1)?"%":"",$myFilters[$k]["values"][0],($myFilters[$k]["exact"]!=1)?"%":"");
					}
                }elseif (array_key_exists('externalFile', $myFilters[$k]) && array_key_exists('externalField', $myFilters[$k])){
					$eData = (array)json_decode(file_get_contents($myFilters[$k]["externalFile"]));
					foreach($eData['tracks'] as $track){
						$fName=$track->$myFilters[$k]["externalField"];
						if(strpos($fName,'.bed')!==false){
							$ayytest[]=sprintf("'%s'",substr($fName,0,strpos($fName,'.bed')));
						}
					}
                	($ct>0)?$whereClause .= " AND ":1;
                  	$whereClause .= "`" . $k . "`" . " IN ";
                  	$whereClause .= "(" . join(",", $ayytest) . ")";
				}elseif (array_key_exists('Min', $myFilters[$k]) ||
                    array_key_exists('Max', $myFilters[$k])
                    ){
                	($ct>0)?$whereClause .= " AND ":1;
                  $rangeCt=0;
                  foreach ($myFilters[$k] as $rangeKey => $rangeOp){
                    ($rangeCt>0)? $whereClause .= " AND ":1;
                    $whereClause .= "`" . $k . "`" ;
                    $whereClause .= $rangeOp;
                    $rangeCt++;
                  }
                }else{
					$encValues=array();
					foreach ($myFilters[$k]["values"] as $val){
						$encValues[]=sprintf("'%s'",$val);
					}
                	($ct>0)?$whereClause .= " AND ":1;
                  	$whereClause .= "`" . $k . "`" . " NOT IN ";
                  	$whereClause .= "(" . join(",", $encValues) . ")";
                }
                $ct++;
              }
           }
			file_put_contents("testout.txt",print_r($myFilters,true));
           
           (DEBUG_ME == 1)?file_put_contents("getjsonq-where_sql.txt",print_r($whereClause,true)):0;
    }

if((isset($_REQUEST["action"]) && $_REQUEST["action"]==="export") || $argv[2]==="export"){
	if(isset($_REQUEST["arrayIDs"]) || $argv[3]==="arrayIDs"){
	$listIDs = $_REQUEST["arrayIDs"];
	$whereClause = sprintf("WHERE `%s` IN(%s)",'CELLID',$listIDs);
	}
}
    
    $sqlStr="SELECT StudyName,su.SID,sav1.value AS IID,sav2.value AS FamID, sav3.value AS OtherID
                                FROM (Study AS st INNER JOIN Subject AS su ON st.STID=su.SubjectSTID)
                                LEFT JOIN Subject_attribute_value AS sav1 ON sav1.SID=su.SID AND sav1.SAID=1
                                LEFT JOIN Subject_attribute_value AS sav2 ON sav2.SID=su.SID AND sav2.SAID=2
                                LEFT JOIN Subject_attribute_value AS sav3 ON sav3.SID=su.SID AND sav3.SAID=29
                                /* LIMIT $rowCount OFFSET $offset*/
                                ";
                                
    $sqlStr="SELECT SMID,StudyName, 
          CONCAT(ConsortiumAbbr,'-',
                StudyID,'-',
                sav1.value) AS ADSPID,
          sav2.value AS FamID, 
          CASE WHEN sm.SMID IS NOT NULL THEN 
                CONCAT(ConsortiumAbbr,'-',
                        StudyID,'-',
                        sav1.value ,'-',
                        sm.SampleSource,'-',
                        sm.SampleRepoID,'-',
                        sm.SampleAliquotID) END AS ADSP_SM_ID, 
          sm.SampleSelection AS Study, 
          sav4.value AS dbGapID, 
          sav3.value AS OtherID,
          sav5.value AS Gender,
          sav6.value AS Consent,
          sav7.value AS Age,
          sav8.value AS APOE,
          sav9.value AS Autopsy,
          sav10.value AS Braak,
          sav11.value AS AD,
          sav12.value AS Race,
          sav13.value AS Ethnicity,
          sav14.value AS lssc,
          /*
          sav15.value AS Sampled,
          sav16.value AS GenomicsSampleAvail,
          sav17.value AS Sequencing,
          sav18.value AS Linkage,
          */
          sav19.value as numlanes,
          sav20.value as avgcov,
          sav21.value as totbases,
          sav22.value as totmapbases,
          sav23.value as totunimapbases,
          sav24.value as percent_aligned,
          sav25.value as percent_unique,
          sav26.value as error_rate,
          sav27.value as total_reads,
          sav28.value as targeted_insert_length,
          dbGapSA.sra_sample_id
        FROM (Study AS st INNER JOIN Subject AS su ON st.STID=su.SubjectSTID
        INNER JOIN Consortium AS c ON c.CID=st.StudyCID )
        LEFT JOIN Sample AS sm ON sm.SampleSID = su.SID
        LEFT JOIN Subject_attribute_value AS sav1 ON sav1.SID=su.SID AND sav1.SAID=1 /* IID */
        LEFT JOIN Subject_attribute_value AS sav2 ON sav2.SID=su.SID AND sav2.SAID=2 /* FamID */
        LEFT JOIN Subject_attribute_value AS sav3 ON sav3.SID=su.SID AND sav3.SAID=29 /* OtherID */
        LEFT JOIN Subject_attribute_value AS sav4 ON sav4.SID=su.SID AND sav4.SAID=32 /* SourceID */

        LEFT JOIN Subject_attribute_value AS sav5 ON sav5.SID=su.SID AND sav5.SAID=5  /* Sex */
        LEFT JOIN Subject_attribute_value AS sav6 ON sav6.SID=su.SID AND sav6.SAID=30 /* Consent */
        
        LEFT JOIN Subject_attribute_value AS sav7 ON sav7.SID=su.SID AND sav7.SAID=10    /* Age */
        LEFT JOIN Subject_attribute_value AS sav8 ON sav8.SID=su.SID AND sav8.SAID=14    /* APOE */
        LEFT JOIN Subject_attribute_value AS sav9 ON sav9.SID=su.SID AND sav9.SAID=15    /* Autopsy */
        LEFT JOIN Subject_attribute_value AS sav10 ON sav10.SID=su.SID AND sav10.SAID=16 /* Braak */
        LEFT JOIN Subject_attribute_value AS sav11 ON sav11.SID=su.SID AND sav11.SAID=17 /* AD */
        LEFT JOIN Subject_attribute_value AS sav12 ON sav12.SID=su.SID AND sav12.SAID=6  /* Race */
        LEFT JOIN Subject_attribute_value AS sav13 ON sav13.SID=su.SID AND sav13.SAID=7  /* Ethnicity */
        
        LEFT JOIN Subject_attribute_value AS sav14 ON sav14.SID=su.SID AND sav14.SAID=37  /* LSSC */
        LEFT JOIN Subject_attribute_value AS sav15 ON sav15.SID=su.SID AND sav15.SAID=19  /* Sampled */
        LEFT JOIN Subject_attribute_value AS sav16 ON sav16.SID=su.SID AND sav16.SAID=20  /* GenomicsSampleAvail */
        LEFT JOIN Subject_attribute_value AS sav17 ON sav17.SID=su.SID AND sav17.SAID=28  /* Sequencing */
        LEFT JOIN Subject_attribute_value AS sav18 ON sav18.SID=su.SID AND sav18.SAID=27  /* Linkage */
        
        LEFT JOIN Subject_attribute_value AS sav19 ON sav19.SID=su.SID AND sav19.SAID=38  /* Num Lanes */
        LEFT JOIN Subject_attribute_value AS sav20 ON sav20.SID=su.SID AND sav20.SAID=39  /* avg cov */
        LEFT JOIN Subject_attribute_value AS sav21 ON sav21.SID=su.SID AND sav21.SAID=40  /* tot bases */
        LEFT JOIN Subject_attribute_value AS sav22 ON sav22.SID=su.SID AND sav22.SAID=41  /* tot map bases */
        LEFT JOIN Subject_attribute_value AS sav23 ON sav23.SID=su.SID AND sav23.SAID=42  /* tot unique map bases */
        LEFT JOIN Subject_attribute_value AS sav24 ON sav24.SID=su.SID AND sav24.SAID=43  /* percent_aligned */
        LEFT JOIN Subject_attribute_value AS sav25 ON sav25.SID=su.SID AND sav25.SAID=44  /* percent_unique */
        LEFT JOIN Subject_attribute_value AS sav26 ON sav26.SID=su.SID AND sav26.SAID=45  /* error_rate */
        LEFT JOIN Subject_attribute_value AS sav27 ON sav27.SID=su.SID AND sav27.SAID=46  /* total_reads */
        LEFT JOIN Subject_attribute_value AS sav28 ON sav28.SID=su.SID AND sav28.SAID=47  /* targeted_insert_length */
        
        LEFT JOIN dbGap_sample_import  AS dbGapSA ON dbGapSA.submitted_sample_id = CONCAT(ConsortiumAbbr,'-',
                        StudyID,'-',
                        sav1.value ,'-',
                        sm.SampleSource,'-',
                        sm.SampleRepoID,'-',
                        sm.SampleAliquotID) /* sra_sample_id */
                        
        $whereClause
        
        /*ORDER BY FamID DESC, Gender, Consent ASC, sra_sample_id */
        ORDER BY sra_sample_id desc
        ";
if(isset($_REQUEST["fsids"])){
    $query = sprintf("SELECT `MID`,`Save_Type` FROM `Saved_Meta` WHERE `MID` IN (%s) AND `Save_Type` = 1;",mysqli_real_escape_string($mysqli,$_REQUEST["fsids"]));
    if ($result = $mysqli->query($query)){
        if(mysqli_num_rows($result)!==0){
            while ($row = $result->fetch_assoc()){
                $tableSets[] = $row["MID"];
            }
            if(count($tableSets)>0){
                $whereClause = sprintf("WHERE `FID` IN (SELECT `File_ID` FROM `Saved_Sets_Data` LEFT JOIN `Saved_Sets` ON `Saved_Sets`.`Set_ID` = `Saved_Sets_Data`.`SID` WHERE `Meta_ID` IN(%s))",implode(",",$tableSets));
            }else{
                //ZERO ROWS RETURNED
                echo "error";
                exit;
            }
        }
    }
}elseif((isset($_REQUEST["table"]) && $_REQUEST["table"]==="cell") || $argv[1]==="cell"){
    $sqlStr = "SELECT * FROM TEST 
		$whereClause 
               $orderClause
              ;";
    file_put_contents("cell_sql_str.txt", print_r($sqlStr,true));
}elseif($_REQUEST["table"]==="ephys" || $argv[1]==="ephys"){
              $orderClause = "ORDER BY SAMPLE_ID";
    $sqlStr = "SELECT * FROM `tbl_EPHYS` 
		$whereClause 
               $orderClause
              ;";
    file_put_contents("ephys_sql_str.txt", print_r($sqlStr,true));
}elseif($_REQUEST["table"]==="image" || $argv[1]==="image"){
              $orderClause = "ORDER BY IMAGE_ID";
    $sqlStr = "SELECT * FROM tbl_IMAGES 
		$whereClause 
               $orderClause
              ;";
    file_put_contents("image_sql_str.txt", print_r($sqlStr,true));
}

    $sqlStr = "SELECT * FROM `files` 
		$whereClause 
               $orderClause
              ;";
               
	$fp = fopen("./files_sql_str.txt","w");
 	fwrite($fp, print_r($sqlStr,true));
	fclose($fp);

    if ($result = $mysqli->query($sqlStr)) {

        $tot= $result->num_rows;
        $result->data_seek($offset);
        
        $ct=0;
             
        for ($res = array(); $tmp = $result->fetch_array(MYSQLI_ASSOC);$ct++){
             $res[] = $tmp;
                 
             /*$res[] = array_merge($tmp, array("dbGapID" => "GAP" . rand(10000,99999),
                                              "Gender"=>$mf[$var1],
                                              "Consent"=>$yn[$var2]
                                            ));
             */
             
             if ($ct>$rowCount-2){break;}
             
        }
    }
	else{
		print $mysqli->error; 
	}
    
    #load full (no-limit) data
    if ($result){
      $result->data_seek(0);    
            #$sliceStart = ($submittedSorts)?:0;
      for ($fullResults = array(); $fullResultRow = $result->fetch_array(MYSQLI_ASSOC);$ct++){
              $fullResults[][$submittedSorts[0]["property"]] = $fullResultRow[$submittedSorts[0]["property"]];
      }
      // Free result set [memory]
      $result->close();
    }else{
      $fullResults[]=array();
      $tot=0;
      $res="";
    }
    
    // Print out json for ExtJS
    $jStr = json_encode($res);
    $jStr2 = json_encode($fullResults);

if((isset($_REQUEST["action"]) && $_REQUEST["action"]==="export") || $argv[2]==="export"){
    $lines[] = implode(",",array_keys($res[0]));
    foreach($res as $row){
        $lines[] = implode(",",$row);
    }
    print implode("\r\n",$lines); 
}else{
    print '{"totalCount":"' . $tot . '","data":' . $jStr . ', "fullDataDump":' . $jStr2 . '}';
    #print '{"totalCount":"' . $tot . '","data":' . $jStr . '}';
}     
    $mysqli->close();
    
?>
