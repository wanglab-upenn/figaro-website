<?php

require_once('lib/mysql_gadb.inc.php'); # get $mysqli

/*
if($argv[	echo "ayy";
	echo "ayy";
}elseif(isset($argv[1])){
	echo "lmao";

}else{
*/

$uid = 1;
$save_meta_map=array("save_name"=>"Save_name",
	"save_desc"=>"Save_Desc",
	"publish"=>"Save_Public",
	"save_type"=>"Save_Type");
$tableFilters = array();
$tableSets = array();

foreach($_REQUEST as $k => $v){
        $reqs[mysqli_real_escape_string($mysqli,$k)] = mysqli_real_escape_string($mysqli,$v);
}


if(isset($_REQUEST["save_type"])){
    $query = sprintf("SELECT `MID` FROM `Saved_Meta` WHERE `Save_Name` = '%s';",$reqs["save_name"]);
    if ($result = $mysqli->query($query) or die ($mysqli->error) ) {
        if(mysqli_num_rows($result)==0){
            foreach($save_meta_map as $k => $v){
                    if($k=="publish"||$k=="save_type"){
                            $insertvals[] = ($reqs[$k]=="set"||$reqs[$k]=="public")?1:0;
                    }else{
                            $insertvals[] = sprintf("'%s'",$reqs[$k]);	
                    }
            }
            $query = sprintf("INSERT INTO `Saved_Meta` (`Save_User_ID`,%s) VALUES (%d,%s);",
                            implode(",",$save_meta_map),
                            $uid,
                            implode(",",$insertvals));
            if ($result = $mysqli->query($query) or die ($mysqli->error)){
                $newMetaID=$mysqli->insert_id;
                if($reqs["save_type"]=="set"){
                    if(isset($reqs["fids"])){
                        $insertCount=0;
                        $fileIDs=explode(",",$reqs["fids"]);
                        $query = sprintf("INSERT INTO `Saved_Sets` (`Meta_ID`) VALUES (%d);",
                            $newMetaID);
                        if ($result = $mysqli->query($query) or die ($mysqli->error)){
                            $savedSetID=$mysqli->insert_id;
                        }
                        foreach($fileIDs as $fid){
                            $query = sprintf("INSERT INTO `Saved_Sets_Data` (`SID`,`File_ID`) VALUES (%d,%d);",
                                $savedSetID,
                                $fid);
                            if ($result = $mysqli->query($query) or die ($mysqli->error)){
                                $insertCount++;
                            }
                        }
                    }else{
                        //NO FILE IDS PASSED
                    }
                }else{
                    if(isset($reqs["fltrJSON"])){
                        $filters=json_decode($reqs["fltrJSONfids"]);
                        $query = sprintf("INSERT INTO `Saved_Filters` (`Meta_ID`) VALUES (%d);",
                            $newMetaID);
                        if ($result = $mysqli->query($query) or die ($mysqli->error)){
                            $savedFilterID=$mysqli->insert_id;
                        }
                        $query = sprintf("INSERT INTO `Saved_Filters_Data` (`FID`,`Filter_Params`) VALUES (%d,'%s');",
                            $savedFilterID,
                            $reqs["fltrJSON"]);
                        if ($result = $mysqli->query($query) or die ($mysqli->error)){
                            $insertCount=count((array)$filters);
                        }
                    }else{
                        //NO FILTERS PASSED
                    }
                }
                printf("{\"success\":true,\"msg\":\"Saved %s named: %s (ID: %d) with %d associated %s.\"}",$reqs["save_type"],$reqs["save_name"],$newMetaID,$insertCount,($reqs["save_type"])?"files":"parameters");
            }
        }else{
            printf("{\"success\":false,\"msg\":\"Save named %s already exists.\"}",$reqs["save_name"]);
        }
    }
}elseif(isset($_REQUEST["fsids"])){
    $query = sprintf("SELECT `Filter_Params`
        FROM `Saved_Meta`
        LEFT JOIN `Saved_Filters` ON `Meta_ID` = `MID`
        LEFT JOIN `Saved_Filters_Data` ON `Filter_ID` = `FID`
        WHERE `Save_Type` = 0
        AND `Meta_ID`
        IN (%s) ",$reqs["fsids"]);
    if ($result = $mysqli->query($query) or die ($mysqli->error) ) {
        if(mysqli_num_rows($result)!==0){
            while ($row = $result->fetch_assoc()){
                $tableFilters[] = $row;
            }
        }
    }
    $jFilters = json_encode($tableFilters);
    print "{\"success\":\"true\",\"filters\":".$jFilters."}";
}else{
    $query = sprintf("SELECT * FROM `Saved_Meta` WHERE `Save_User_ID` = %d ORDER BY `Save_Type` ASC;",$uid);
    if ($result = $mysqli->query($query) or die ($mysqli->error) ) {
        if(mysqli_num_rows($result)!==0){
            while ($row = $result->fetch_assoc()){
                $tableFilters[] = $row;
            }
        }
    }
    $jFilters = json_encode($tableFilters);
    $jSets = json_encode($tableSets);
    print "{\"success\":\"true\",\"saves\":".$jFilters."}";
}
 
$mysqli->close();

?>
