<?php

require_once('lib/mysql_gadb.inc.php'); # get $mysqli

/*
if($argv[	echo "ayy";
	echo "ayy";
}elseif(isset($argv[1])){
	echo "lmao";

}else{
*/

$uid = 1;
$save_meta_map=array("save_name"=>"Save_name",
	"save_desc"=>"Save_Desc",
	"publish"=>"Save_Public",
	"save_type"=>"Save_Type");
$userRegion = array();
$tableSets = array();

foreach($_REQUEST as $k => $v){
        $reqs[mysqli_real_escape_string($mysqli,$k)] = mysqli_real_escape_string($mysqli,$v);
}


if(isset($reqs["action"])){
        if($reqs["action"]=="export"){
            $setFields=array();
            $dupeVals=array();
            if(isset($reqs["region"])){
                $setFields["`Region`"]=$reqs["region"];
            }
            if(isset($reqs["filters"])){
                $setFields["`Filters`"]=(!empty($reqs["filters"]))?$reqs["filters"]:"null";
            }
            foreach($setFields as $k => $v){
                $dupeVals[]=sprintf("%s=%s",$k,(!empty($reqs["filters"]))?"'".$v."'":"null");
            }
            if(!empty($setFields)){
                $query = sprintf("INSERT INTO `Session_Meta` (`Session_User_ID`,%s) VALUES (%d,'%s') ON DUPLICATE KEY UPDATE %s;",
                                implode(array_keys($setFields)),
                                $uid,
                                implode("','",$setFields),
                                implode($dupeVals));
                if ($result = $mysqli->query($query) or die ("{success\":\"false\",\"msg\":\"".$mysqli->error.": ".$query."\"}")){
                    print "{\"success\":\"true\",\"msg\":\"User specified region updated successfully.\"}";
                }
            }
        }
}elseif(isset($reqs["init"]) or $argv[1]==="init")
    $query = sprintf("SELECT `Region`,`Filters`
        FROM `Session_Meta`
        WHERE `Session_User_ID` = %d",1);
    if ($result = $mysqli->query($query) or die ($mysqli->error) ) {
        if(mysqli_num_rows($result)!==0){
            while ($row = $result->fetch_assoc()){
                $userRegion[] = $row;
            }
        }
    }
    $jFilters = json_encode($userRegion);
    print "{\"success\":\"true\",\"Init\":".$jFilters."}";
}else{
    $query = sprintf("SELECT * FROM `Session_Meta` WHERE `Session_User_ID` = %d ORDER BY `Session_Last` ASC;",$uid);
    if ($result = $mysqli->query($query) or die ($mysqli->error) ) {
        if(mysqli_num_rows($result)!==0){
            while ($row = $result->fetch_assoc()){
                $tableFilters[] = $row;
            }
        }
    }
    $jFilters = json_encode($tableFilters);
    $jSets = json_encode($tableSets);
    print "{\"success\":\"true\",\"saves\":".$jFilters."}";
}
 
$mysqli->close();

?>
