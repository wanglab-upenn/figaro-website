<?php
    require_once('lib/mysql_gadb.inc.php'); # get $mysqli
    define('DEBUG_ME',1);

    $rowCount = (empty($_REQUEST['limit']) ? 50 : $_REQUEST['limit']);
    $offset = (empty($_REQUEST['start']) ? 0 : $_REQUEST['start']);
    $totals = 0;
    
    $orderClause="ORDER BY `Identifier`";

    $reqs = $_REQUEST;
    if(isset($reqs["src"])){
        $outsource=array();
        foreach(json_decode($reqs["src"]) as $jObj){
            $outsource[] = $jObj->url;
        }
        $sources = $outsource;
        (!isset($reqs["sort"]))?$reqs["sort"]="[{\"property\":\"FID\",\"direction\":\"ASC\"}]":0;
    }else{
        $sources = explode("|",$reqs["sources"]);
    }
    $trueSort = json_decode($reqs["sort"]);
    $params = (count($sources)>1 && count($trueSort))?array("check=true"):array();
    
    foreach($reqs as $k => $v){
        ($k!="sources")?$params[]=sprintf("%s=%s",str_replace(" ","%20",$k),str_replace(" ","%20",$v)):0;
    }

    #print_r($reqs);

    #($reqs["src"])?print_r(json_decode($reqs["src"])):0;

    #$reqs["sources"] = "http://gadb.niagads.org|http://gadb.lisanwanglab.org";

    $jsonData = array();
    $ayy = array();
    $pullSource = array();

    if($reqs["action"]=="import"&&isset($reqs["src"])){
        foreach(json_decode($reqs["src"]) as $srcObj){
            foreach($srcObj->fids as $fid){
                $totals++;
                $ayy[] = array("FID"=>$fid,"Source URL"=>$srcObj->url);
            }
        }
    }else{
        foreach($sources as $source){
            $jsonStr = "";
            $parsedURL = sprintf("%s/getJSONNewQuery.php?%s",$source,implode("&",$params));
            $pullSource[] = $parsedURL;
            #echo $parsedURL;
            $handle = fopen($parsedURL, "r");
            while(!feof($handle)){
                $jsonStr .= fread($handle,32768);
            }
            fclose($handle);
            $jsonData[$source] = json_decode($jsonStr,true);
        }

        foreach($jsonData as $k => $src){
            $tmpSrc = array();
            foreach($src["data"] as $row){
                $tmpRow = $row;
                $tmpRow["Source URL"] = $k;
                $tmpSrc[] = $tmpRow;
            }
            $totals += $src["totalCount"];
            $ayy = array_merge($ayy,$tmpSrc);
        }

        #echo json_encode($ayy);

        if($trueSort[0]->direction==="ASC"){
            usort($ayy,function($a,$b) use ($trueSort){ return strcasecmp($a[$trueSort[0]->property],$b[$trueSort[0]->property]); });
        }else{
            usort($ayy,function($a,$b) use ($trueSort){ return strcasecmp($b[$trueSort[0]->property],$a[$trueSort[0]->property]); });
        }
    }

    if(count($sources)>1 && count($trueSort)){
        foreach(array_slice($ayy,$offset,$rowCount) as $ayyrow){
            $fids[$ayyrow["Source URL"]][] = $ayyrow["FID"];
        }

        $jsonData = array();

        foreach($fids as $source => $fidList){
            $jsonStr = "";
            $parsedURL = sprintf("%s/getJSONNewQuery.php?fids=%s",$source,implode(",",$fidList));
            $handle = fopen($parsedURL, "r");
            while(!feof($handle)){
                $jsonStr .= fread($handle,32768);
            }
            fclose($handle);
            $jsonData[$source] = json_decode($jsonStr,true);
        }
        
        $ayy = array();

        foreach($jsonData as $k => $src){
            $tmpSrc = array();
            foreach($src["data"] as $row){
                $tmpRow = $row;
                $tmpRow["Source URL"] = $k;
                $tmpRow["debug"] = $src["debug"];
                $tmpSrc[] = $tmpRow;
            }
            $ayy = array_merge($ayy,$tmpSrc);
        }

        if($trueSort[0]->direction==="ASC"){
            usort($ayy,function($a,$b) use ($trueSort){ return strcasecmp($a[$trueSort[0]->property],$b[$trueSort[0]->property]); });
        }else{
            usort($ayy,function($a,$b) use ($trueSort){ return strcasecmp($b[$trueSort[0]->property],$a[$trueSort[0]->property]); });
        }
    }
    
    $debugs = array();

    foreach($jsonData as $k => $src){
        $debugs[] = $src["debug"]; 
    }

    #echo json_encode($ayy);

    #echo json_encode($ayy,true);

    #print_r($jsonData["data"])
    
    #echo json_encode($fids);
    echo json_encode(array("totalCount" => $totals, "self-debug" => implode("|",$pullSource), "debug" => implode("|",$debugs), "data" => $ayy));
    #echo json_encode(array_slice($ayy,$offset,$rowCount));

?>
