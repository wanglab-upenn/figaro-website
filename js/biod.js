function collapsePane(showID,hideID,showBoth){
	if(typeof showBoth === 'undefined'){
		Ext.getCmp(hideID).hide();Ext.getCmp(showID).show();
	}else{
		Ext.getCmp(hideID).show();Ext.getCmp(showID).show();
	}
}

function styleChange(smID,styleIN){
	//var objIndex = bdw.sources.index[smID];
	//bdw.sources.data[objIndex].style = bigVars.figaro.formatting[styleIN]; 
	//dump(smID);
	//dump(styleIN);
		/*
		[
		{type: 'default',
			style: {
				glyph: 'ARROW',
				HEIGHT: '12',
				//STROKECOLOR:'black',
				FGCOLOR: styleIN}
		}];
		*/
	loadBioD();
}

function cleanEle(contID){
	Ext.getCmp(contID).items.clear();
	Ext.getCmp(contID).update();
	Ext.getCmp(contID).doLayout();
	return 1;
}

function prepEle(contID){
	Ext.getCmp(contID).add(bigVars.figaro.form4);
	Ext.getCmp(contID).update();
	Ext.getCmp(contID).doLayout();
	return 1;
}

function initSources(){
	bdw.selects=[];
	bdw.genomeVer={name:'hg19',version:'37'};
	bdw.genomes={'hg19':[],'GRCh38':[]};
	bdw.sources={'index':{'Genome':0},
			'data':[{id: 'Genome',
			name: 'Genome',
        	twoBitURI: 'http://faraday.pcbi.upenn.edu/Annotationtracks/Genome/hg19.2bit',
            tier_type: 'sequence'}]};
	return 1;
}

function qButtons(rowmeta,smID){
        //alert(smID);
	var actX = rowmeta.substring(0,rowmeta.search('_'));
	var rID = rowmeta.substring(rowmeta.search('_')+1);
	if(actX==='add'){
		if(typeof smID!=='undefined'){
			getSelectsForBioD(rID);
			$.notify({message:'Sample '+smID+' Added to Cart...'},{type:'success',delay:2000,offset:10});
		}else{
			$.notify({message:'This shouldnt happen.'},{type:'danger',delay:5000,offset:10});
		}
        }else if(actX==='drop'){
		if(typeof smID!=='undefined'){
                        Ext.getCmp('figSelectedGrid').store.removeAt(Ext.getCmp('figSelectedGrid').store.indexOfId(smID));
			bdw.sources.data.splice(bdw.sources.data.indexOf(bdw.sources.data.find(a => a.id === smID)),1);
			bdw.selects.splice(bdw.selects.indexOf(bdw.selects.find(a => a.id === smID)),1);
                        bdw.genomes.hg19.splice(bdw.genomes.hg19.indexOf(smID),1)
                        loadBioD();
		}else{
			$.notify({message:'This shouldnt happen.'},{type:'danger',delay:5000,offset:10});
		}
	}else{
		if(initSources()){
			if(actX==='pop'){
				//drawBioDalModal(rID);
			}else{
				//drawBioDalPanel(rID);
			}
		}
	}
}

function getSelectsForBioD(rowSampleID){
	var arrayList=[];
	if(typeof rowSampleID==='undefined'){
		var selected=Ext.getCmp('figTableGrid').getView().getSelectionModel().getSelection();
	}else{
		var selected=[Ext.getCmp('figTableGrid').getStore().getAt(rowSampleID)];
	}
	Ext.each(selected, function (item) {
	   arrayList.push(item.data);
           bdw.genomes[item.data['Genome build']].push(item.data.Identifier);
	});
        //dump(bdw.genomes);
        if(bdw.genomes.hg19.length>0&&bdw.genomes.GRCh38.length>0){
            alert('Mixing genome builds is not allowed.');
        }else{
            if(bdw.genomes.GRCh38>0){
                bdw.sources.data[0].twoBitURI='http://faraday.pcbi.upenn.edu/Annotationtracks/Genome/hg38.2bit';
                bdw.genomeVer.version='38';
                bdw.genomeVer.name='hg38';
            }else{
                bdw.sources.data[0].twoBitURI='http://faraday.pcbi.upenn.edu/Annotationtracks/Genome/hg19.2bit';
                bdw.genomeVer.version='37';
                bdw.genomeVer.name='hg19';
            }
	    arrayList.forEach(function(item,index){
                if(item['File path'].search('http')===0){
                    var URIStr = item["File path"]+'/'+item["File name"];
                }else{
                    var tmpURIStr = item['File path'].substring(item['File path'].search('GenomeBrowserTracks'));
                    var URIStr = 'http://tesla.pcbi.upenn.edu/gbt'+tmpURIStr.substring(tmpURIStr.search('/'))+'/'+item['File name'];
                }
		bdw.selects.push({id:item.Identifier,
			fname:item['File name'],
			dsource:item['Data Source'],
			ctype:item['Cell type'],
			style:'Default Style...'});
		if(typeof bdw.sources.index[item.Identifier]==='undefined'){
				bdw.sources.index[item.Identifier]=bdw.sources.data.length;
                                var bdObj ={
                                    id:item.Identifier,
                                    name:item['File name'],
                                    desc:item['Data Source']+', '+item['Cell type']
                                };
                                if(item['File format'].search('bam')!=-1){
                                    bdObj.bamURI=URIStr;
                                }else if(item['File format'].search('bed')!=-1){
                                    bdObj.uri=URIStr;
                                    bdObj.tier_type='tabix';
                                    bdObj.payload='bed';
                                }else if(item['File format'].search('2bit')!=-1){
                                    bdObj.twoBitURI=URIStr;
                                    bdObj.tier_type='sequence';
                                }else{
                                    bdObj.bwgURI=URIStr;
                                }
				bdw.sources.data.push(bdObj);
		}else{
			bdw.sources.data[bdw.sources.index[item.Identifier]]={
                                id:item.Identifier,
				name:item['File name'],
				desc:item['Data Source']+', '+item['Cell type'],
				bwgURI:URIStr}
		}
	    });
        }
	Ext.getCmp('figSelectedGrid').store.loadData(bdw.selects);
	return 1;
}

function loadBioD(){
bdw.brwsrObj = new Browser({
    chr:                 bigVars.figaro.bdregion.chr,
    viewStart:           parseInt(bigVars.figaro.bdregion.viewStart,10),
    viewEnd:             parseInt(bigVars.figaro.bdregion.viewEnd,10),
    cookieKey:           'human-grc_h37',

    coordSystem: {speciesName: 'Human',
      speciesName: 'Human',
      taxon: 9606,
      auth: 'GRCh',
      version: bdw.genomeVer.version,
      ucscName: bdw.genomeVer.name
    },

    chains: {
      hg18ToHg19: new Chainset('http://www.derkholm.net:8080/das/hg18ToHg19/', 'NCBI36', 'GRCh37',
                               {
                                  speciesName: 'Human',
                                  taxon: 9606,auth: 'NCBI',
                                  auth: 'NCBI',
                                  version: 36,
                                  ucscName: 'hg18'
                               })
    },

    sources:          bdw.sources.data,

/*
                       {name: 'Genes',
                       desc: 'Gene structures from GENCODE 19',
                       bwgURI: 'http://bd.lisanwanglab.org/barry_files/test_html/gencode.v19.annotation.bed.bb',
                       stylesheet_uri: 'http://bd.lisanwanglab.org/barry_files/test_html/stylesheets/gencode_from_bd.xml',
                       collapseSuperGroups: true,
                       trixURI: 'http://bd.lisanwanglab.org/barry_files/test_html/datasets/geneIndex.ix'},

                       {name: 'Pseudogenes',
                       desc: 'Consensus pseudogenes predicted by the Yale and UCSC pipelines / GENCODE 19',
                       bwgURI: 'http://bd.lisanwanglab.org/barry_files/test_html/gencode.v19.2wayconspseudos.bed.bb'},

                       {name: 'Long Noncoding RNAs',
                       desc: 'Long non-coding RNA gene annotation / GENCODE 19',
                       bwgURI: 'http://bd.lisanwanglab.org/barry_files/test_html/gencode.v19.long_noncoding_RNAs.gtf.bed.bb'},

                      {name: 'tRNAs',
                       desc: 'Predicted tRNA genes / GENCODE 19',
                       bwgURI: 'http://bd.lisanwanglab.org/barry_files/test_html/gencode.v19.tRNAs.bed.bb'},

                       {name: 'PolyAs',
                       desc: 'PolyA feature annotation / GENCODE 19',
                       bwgURI: 'http://bd.lisanwanglab.org/barry_files/test_html/gencode.v19.polyAs.bed.bb'}
*/
              
    prefix: '//www.biodalliance.org/release-0.13/',
    fullScreen: true,
    maxViewWidth: 5000000,
    browserLinks: {
      Ensembl: 'http://www.ensembl.org/Homo_sapiens/Location/View?r=${chr}:${start}-${end}',
      UCSC: 'http://genome.ucsc.edu/cgi-bin/hgTracks?db=hg19&position=chr${chr}:${start}-${end}',
      Sequence: 'http://www.derkholm.net:8080/das/hg19comp/sequence?segment=${chr}:${start},${end}'
    },

    hubs: ['http://ngs.sanger.ac.uk/production/ensembl/regulation/hub.txt', 'http://ftp.ebi.ac.uk/pub/databases/ensembl/encode/integration_data_jan2011/hub.txt'],
  });
	//for(i=0;i<document.getElementsByClassName('track-label').length;i++){dump(document.getElementsByClassName('track-label')[i].children[0].innerText);}
}
