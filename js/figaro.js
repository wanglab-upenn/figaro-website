var ayylamo = "";

function waitForDelay(){
    return 1;
}

function getObj(objID){
    return document.getElementById(objID);
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function dump(obj) {
    var out = '';
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }
    alert(out);
}

function pullSources(){
    var sources = [];
    var menuObj = Ext.getCmp(prefix.figaro+'SourceMenu').menu;
    Ext.getCmp(prefix.figaro+'TableGrid').store.removeAll();
    Ext.getCmp(prefix.figaro+'SourceMenu').menu.items.items.forEach(function(rec){
        if(rec.checked==true){
            sources.push(rec.value)
        }
    });
    Ext.getCmp(prefix.figaro+'TableGrid').store.proxy.extraParams={'sources':sources.join('|')};
    Ext.getCmp(prefix.figaro+'TableGrid').store.load();
}

function makeLoadMenu(menuID){
	Ext.create('Ext.data.JsonStore',{
		id:'testjson',
		fields: ['MID','Save_Name','Save_Desc','Save_Type'],
		proxy: {
			type: 'ajax',
			url: prefix.source+'/getJSONSaves.php',
			reader: {
				type: 'json',
				root: 'saves'
			}
		},
		displayField: 'Save_Name',
		valueField: 'MID',
		autoLoad:false,
		listeners:{
			refresh:function(){
                            var lastVal = '0';
                            var menuObj = Ext.getCmp(menuID).menu;
                            (menuObj.items.length>0)?menuObj.removeAll():{};
                            this.data.items.forEach(function(rec){
                            (lastVal!=rec.data['Save_Type'])?menuObj.add({xtype: 'menuseparator'}):{};
                            lastVal=rec.data['Save_Type'];
                            menuObj.add({
                                text:rec.data['Save_Name']+' | '+((rec.data['Save_Type']==='1')?'Set':'Filter'),
                                mysqlField:'MID',
                                value:rec.data['MID'],
                                checked:false});	
                            });
			}
		}
	}); 
}

function drawBioDalModal(rowID){
	if(!Ext.getCmp(prefix.figaro+'BioDalExportModal')){
		Ext.create('Ext.window.Window', {
		id      : prefix.figaro+'BioDalExportModal',
		modal      : true,
		centered   : true,
		scrollable : true,
		width      : '90%',
		height     : '90%',
		layout     : {
			type  : 'hbox',
			pack  : 'start',
			align : 'stretch'
		},
		items      : bigVars.figaro.form4,
		listeners: {
			beforerender: function(){
				if(Ext.getCmp(prefix.figaro+'BioDalExportPanel')){
					Ext.getCmp(prefix.figaro+'BioDalExportPanel').hide();
					cleanEle(prefix.figaro+'BioDalExportPanel');
				}
			},
			afterrender: function(){
				(typeof rowID==='undefined')?getSelectsForBioD():getSelectsForBioD(rowID);
			}
		}
		}).show();
	}else{
		if(Ext.getCmp(prefix.figaro+'BioDalExportPanel')){
			Ext.getCmp(prefix.figaro+'BioDalExportPanel').hide();
			cleanEle(prefix.figaro+'BioDalExportPanel');
		}
		Ext.getCmp(prefix.figaro+'BioDalExportModal').show();	
		cleanEle(prefix.figaro+'BioDalExportModal');
		prepEle(prefix.figaro+'BioDalExportModal');
		(typeof rowID==='undefined')?getSelectsForBioD():getSelectsForBioD(rowID);
	}
}

function drawBioDalPanel(rowID){
	if(!Ext.getCmp(prefix.figaro+'BioDalExportPanel')){
		Ext.getCmp(prefix.figaro+'GridFrame').add({
		xtype: 'container',
		id      : prefix.figaro+'BioDalExportPanel',
		scrollable : true,
		layout     : {
			type  : 'hbox',
			pack  : 'start',
			align : 'stretch'
		},
		height: 500,
		items      : bigVars.figaro.form4,
		listeners: {
			afterrender: function(){
				//dump(rowID);
				(typeof rowID==='undefined')?getSelectsForBioD():getSelectsForBioD(rowID);
			}
		}
		});
	}else{
		Ext.getCmp(prefix.figaro+'BioDalExportPanel').show();	
		cleanEle(prefix.figaro+'BioDalExportPanel');
		prepEle(prefix.figaro+'BioDalExportPanel');
		(typeof rowID==='undefined')?getSelectsForBioD():getSelectsForBioD(rowID);
	}
}

function drawJBrowsePanel(rowID){
	if(!Ext.getCmp(prefix.figaro+'BioDalExportPanel')){
		Ext.getCmp(prefix.figaro+'GridFrame').add({
		xtype: 'container',
		id      : prefix.figaro+'JBrowseExportPanel',
		scrollable : true,
		layout     : {
			type  : 'hbox',
			pack  : 'start',
			align : 'stretch'
		},
		height: 500,
		items      : bigVars.figaro.form6,
		listeners: {
			afterrender: function(){
                                var tracks=[];
				var Set_ID = ['DNA'];
				var selected=Ext.getCmp('figTableGrid').getView().getSelectionModel().getSelection();
				Ext.each(selected, function (item) {
				   tracks.push(item.data['File name']);
				});
				getObj('jbHolder').innerHTML='<iframe src="http://jbrowse-dev.lisanwanglab.org/?loc=chr'+bigVars.figaro.bdregion.chr+'%3A'+bigVars.figaro.bdregion.viewStart+'..'+bigVars.figaro.bdregion.viewEnd+'&tracks='+tracks.join('%2C')+'&highlight=" style="width:100%;height:500px;border:0;">Please Wait...</iframe>';
			}
		}
		});
	}else{
		Ext.getCmp(prefix.figaro+'BioDalExportPanel').show();	
		cleanEle(prefix.figaro+'BioDalExportPanel');
		prepEle(prefix.figaro+'BioDalExportPanel');
		(typeof rowID==='undefined')?getSelectsForBioD():getSelectsForBioD(rowID);
	}
}

function resetFilters(){
    Ext.getCmp(prefix.figaro+'dFilters').removeAll();
    Ext.getCmp(prefix.figaro+'TableGrid').getStore().clearFilter();
    /*
    Ext.Ajax.request({
       url:prefix.source+'getJSONSession.php',
       params: {'action':'set','filters':null},
       success: function(response){
            var parsedResponse = JSON.parse(response.responseText);
            $.notify({message: 'Successfully Reset Filters: '},{type:'success',delay:2000,offset:10});
       },
       failure: function ( result, request ) {
            $.notify({message:'An Error Occurred.'},{type:'warning',delay:2000,offset:10});
       }
    });
    */
};

Ext.override(Ext.Window, {
    closeAction: 'hide'
});

var testvar = "";
var bdw = {}; //BioDaliance Window obj

var prefix = {'figaro':'fig','source':'http://gadb.niagads.org'};
var bigVars = {'figaro':{
	'prefix':'fig',
	'bdregion':{
		chr:'22',
		viewStart:30700000,
		viewEnd:30900000
	},
	'filters':{
		'filterIDs':[],
		'checkItems':[
			{'objID':prefix.figaro+'FltDataSource'},
			{'objID':prefix.figaro+'FltAssay'},
			{'objID':prefix.figaro+'FltDataType'},
			{'objID':prefix.figaro+'FltFileCompat'},
		],
		'rangeBoxes':[],
		'searchFields':[
			{'objID':prefix.figaro+'FltFilename'},
			{'objID':prefix.figaro+'FltAntibody'},
			{'objID':prefix.figaro+'FltCellType'},
		],
		'configs':[],
	},
	'formatting':{
		'dot plot':{style: [{type: 'default', style: {glyph: 'DOT', FGCOLOR: 'rgb(166,71,71)', HEIGHT: 30, SCATTER: true, SIZE: 3 }}]},
		'line plot':{style: [{type: 'default', style: {glyph: 'LINEPLOT', BGCOLOR: 'rgb(8,104,172)', HEIGHT: 30, id: 'style1'}}]},
		'histogram':{style: [{type: 'default', style: {glyph: 'HISTOGRAM', BGCOLOR: 'rgb(8,104,172)', HEIGHT: 30, id: 'style1'}}]},
		'intervals/segmentation':{style: [{type: 'default', style: {glyph: 'BOX', FGCOLOR: 'black', BGCOLOR: 'red', HEIGHT: 10, BUMP: false, LABEL: false, BGITEM: true, id: 'style1'}}]},
		'Genecode':{style: [{type: 'transcript', method: '.*pseudogene.*', style: {glyph: 'BOX', HEIGHT: 10, FGCOLOR: 'blue' , BGCOLOR: 'white', BUMP: true, LABEL: true, id: 'style6' } },
                    {type: 'translation', method: '.*pseudogene.*', style: {glyph: 'BOX', HEIGHT: 10, FGCOLOR: 'blue' , BGCOLOR: 'blue', id: 'style7' } },
                    {type: 'transcript', method: '.*RNA.*', style: {glyph: 'BOX', HEIGHT: 10, FGCOLOR: 'purple' , BGCOLOR: 'purple', BUMP: true, LABEL: true, id: 'style8' } },
                    {type: 'translation', method: '.*RNA.*', style: {glyph: 'BOX', HEIGHT: 10, FGCOLOR: 'purple' , BGCOLOR: 'purple', id: 'style9' } },
                    {type: 'transcript', style: {glyph: 'BOX', HEIGHT: 10, FGCOLOR: 'black' , BGCOLOR: 'white', BUMP: true, LABEL: true, id: 'style10' } },
                    {type: 'translation', style: {glyph: 'BOX', HEIGHT: 10, FGCOLOR: 'black' , BGCOLOR: 'red', id: 'style11' } }
                ]},
	},
	'form1':{
            title: 'Filter Builder',
            layout: {
                    type: 'vbox',
                    align:'stretch',
                    pack: 'start'
            },
            //flex: 2,
            border: false,
            items: [{
            xtype: 'fieldcontainer',
            id: prefix.figaro+'bigtest',
            layout: {
                type: 'vbox',
            },
            padding: '4 0 0 1',
            border: false,
            defaultType: 'textfield',
            defaults: {
                width: 170
            },
            fieldDefaults: {
                labelAlign: 'top',
                labelStyle: 'font-weight:bold',
                border:false,
            },
            items: [
                {
                xtype: 'toolbar',
                dock: 'top',
                border: 'none',
                items: [{
                        text: 'Data Source',
                        id: prefix.figaro+'FltDataSource',
                        menu: [
                            {text: 'ENCODE',checked:true, mysqlField:'Data Source',mysqlValue:'ENCODE'},
                            {text: 'FANTOM5',checked:true, mysqlField:'Data Source',mysqlValue:'FANTOM5'},
                            {text: 'Reference assembly',checked:true, mysqlField:'Data Source',mysqlValue:'Reference assembly'},
                        ]
                }]
                },{
                    xtype: 'toolbar',
                    dock: 'top',
                    border: 'none',
                    items: [{
                            text: 'Assay',
                            id: prefix.figaro+'FltAssay',
                            menu: [
                                    {text: 'Gene expression (Affymetrix Exon 1.0 ST Arrays)',checked: true, mysqlField:'Assay',mysqlValue:'Gene expression (Affymetrix Exon 1.0 ST Arrays)'},
                                    {text: 'DnaseSeq',checked: true, mysqlField:'Assay',mysqlValue:'DnaseSeq'},
                                    {text: 'ChipSeq',checked: true, mysqlField:'Assay',mysqlValue:'ChipSeq'},
                                    {text: 'N/A',checked: true, mysqlField:'Assay',mysqlValue:'NA'},
                            ]
                    }]
                },{
                    xtype: 'toolbar',
                    dock: 'top',
                    border: 'none',
                    items: [{
                            text: 'File Compatibility',
                            id: prefix.figaro+'FltFileCompat',
                            menu: [
                                    {text:'BioDaliance',checked:true, mysqlField:'File format',mysqlValue:['.bb','.bw','.bam','.2bit']},
                                    {text:'jBrowse',checked:true, eFile:'http://jbrowse.lisanwanglab.org/data/trackList.json',eField:'label',mysqlField:'File name'},
                                    {text:'Other',checked:true, mysqlField:'File format',mysqlValue:['.csv','.gff','.rda','.narrowPeak','.tsv','.txt','.tbi','.tsv','.vcf']},
                            ]
                    }]
                },{
                    xtype: 'fieldset',
                    id: prefix.figaro+'FltFilename',
                    title: 'Filename',
                    collapsible: true,
                    collapsed: true,
                    defaults:{
                            xtype:'textfield',
                    },
                    items:[
                            {mysqlField: 'File name',emptyText:'Search'},
                            {id: prefix.figaro+'FltFilenameExact',name: 'fltFilenameExact',xtype:'checkbox',boxLabel:'Exact',checked:false,},
                            {id: prefix.figaro+'FltFilenameExclude',name: 'fltFilenameExclude',xtype:'checkbox',boxLabel:'Exclude',checked:false,},
                    ],
                },{
                    xtype: 'fieldset',
                    id: prefix.figaro+'FltAntibody',
                    title: 'Antibody',
                    collapsible: true,
                    collapsed: true,
                    defaults:{
                            xtype:'textfield',
                    },
                    items:[
                            {mysqlField: 'Antibody',emptyText:'Search'},
                            {id: prefix.figaro+'FltAntibodyExact',name: 'fltAntibodyExact',xtype:'checkbox',boxLabel:'Exact',checked:false,},
                            {id: prefix.figaro+'FltAntibodyExclude',name: 'fltAntibodyExclude',xtype:'checkbox',boxLabel:'Exclude',checked:false,},
                    ],
                },{
                    xtype: 'fieldset',
                    id: prefix.figaro+'FltCellType',
                    title: 'Cell Type',
                    collapsible: true,
                    collapsed: true,
                    defaults:{
                            xtype:'textfield',
                    },
                    items:[
                            {mysqlField: 'Cell Type',emptyText:'Search'},
                            {id: prefix.figaro+'FltCellTypeExact',name: 'fltCellTypeExact',xtype:'checkbox',boxLabel:'Exact',checked:false,},
                            {id: prefix.figaro+'FltCellTypeExclude',name: 'fltCellTypeExclude',xtype:'checkbox',boxLabel:'Exclude',checked:false,},
                    ],
                }
            ],
      },{
            xtype: 'fieldcontainer',
            id: prefix.figaro+'dFilters',
            layout: {
                type: 'vbox',
            },
            padding: '4 0 0 1',
            border: false,
            defaultType: 'textfield',
            defaults: {
                width: 170
            },
            fieldDefaults: {
                labelAlign: 'top',
                labelStyle: 'font-weight:bold',
                border:false,
            },
      },{
            xtype: 'fieldcontainer',
            id: prefix.figaro+'addFilterBox',
            layout: {
                type: 'vbox',
            },
            padding: '4 0 0 1',
            border: false,
            defaultType: 'textfield',
            defaults: {
                width: 170
            },
            fieldDefaults: {
                labelAlign: 'top',
                labelStyle: 'font-weight:bold',
                border:false,
            },
      }],
      buttons: [{
          text: 'Apply Filters',
          scope: this,
          handler: function(){
            var exclusions=[];
            passedVars = bigVars.figaro.filters;

            for(m=0;m<passedVars.searchFields.length;++m){
                if(Ext.getCmp(passedVars.searchFields[m].objID)){
                    var myObj=Ext.getCmp(passedVars.searchFields[m].objID);
                    for (n=0;n< myObj.items.length;++n){
                        var itm=myObj.items.items[n];
                        //dump(itm);
                        if(typeof itm.value!=='undefined'&&itm.value!==''&&itm.id.indexOf('Exact')===-1&&itm.id.indexOf('Exclude')===-1){
                            exclusions.push({property:itm.mysqlField,value:itm.value,type:'text',exact:myObj.items.items[myObj.items.indexOfKey(myObj.id+'Exact')].checked,incExc:myObj.items.items[myObj.items.indexOfKey(myObj.id+'Exclude')].checked});
                            //dump(itm);
                        }
                    }
                }
            }

            for(m=0;m<passedVars.checkItems.length;++m){
                if(Ext.getCmp(passedVars.checkItems[m].objID)){
                    var myObj=Ext.getCmp(passedVars.checkItems[m].objID);
                    for (n=0;n< myObj.menu.items.length;++n){
                        var itm=myObj.menu.items.items[n];
                        if(!itm.checked){
                            if(Array.isArray(itm.mysqlValue)){
                                for(var i=0;i<itm.mysqlValue.length;i++){
                                    exclusions.push({property:itm.mysqlField,value:itm.mysqlValue[i]});
                                }
                            }else{
                                var pushObj={property:itm.mysqlField,value:itm.mysqlValue};
                                if(typeof itm.eFile!=='undefined'&&typeof itm.eField!=='undefined'){
                                    pushObj.eFile=itm.eFile;
                                    pushObj.eField=itm.eField;
                                }
                                exclusions.push(pushObj);
                            }
                        }
                    }
                }
            }

            for(m=0;m<passedVars.filterIDs.length;++m){
                var myObj=Ext.getCmp(passedVars.filterIDs[m].objID);
                if (myObj.value != null){
                    for (n=0;n< myObj.store.data.items.length;++n){
                        var val=myObj.store.data.items[n].raw[0];
                        if(val != myObj.value ){
                            exclusions.push({property:myObj.mysqlField, value:val});
                        }
                    }
                }
            }

            for(m=0;m<passedVars.rangeBoxes.length;++m){
                var myObj=Ext.getCmp(passedVars.rangeBoxes[m]);
                var valueList=new Object();
                valueList.count=0;

                for (n=0;n<myObj.items.length;n++){
                    var fld = myObj.items.items[n];
                    if (fld.value != null){
                        if (fld.fieldLabel=='Min'){
                            valueList.Min = fld.value;
                            valueList.count++;
                        }
                        if (fld.fieldLabel=='Max'){
                            valueList.Max = fld.value;
                            valueList.count++;
                        }
                    }
                }
                if (valueList.count>0){
                    exclusions.push({property:myObj.mysqlField, value:valueList});
                }
            }

            var gs = Ext.getCmp(prefix.figaro+'TableGrid').getStore();

            if (exclusions.length>0){
                gs.clearFilter(true);
                gs.filter(exclusions);            
                //set session
                /*
                Ext.Ajax.request({
                   url:prefix.source+'/getJSONSession.php',
                   params: {'action':'set','filters':JSON.stringify(Ext.getCmp('figTableGrid').getStore().filters.items)},
                   success: function(response){
                        var parsedResponse = JSON.parse(response.responseText);
                        $.notify({message: 'Successfully Loaded Region: '+parsedResponse.msg},{type:'success',delay:2000,offset:10});
                        //bigVars.figaro.bdregion.chr=usrReg.substr(0,usrReg.search(':'));
                        //bigVars.figaro.bdregion.viewStart=usrReg.substr(usrReg.search(':')+1).split('-')[0];
                        //bigVars.figaro.bdregion.viewEnd=usrReg.substr(usrReg.search(':')+1).split('-')[1];
                        //getObj('region_chr').value=bigVars.figaro.bdregion.chr;
                        //getObj('region_viewStart').value=bigVars.figaro.bdregion.viewStart;
                        //getObj('region_viewEnd').value=bigVars.figaro.bdregion.viewEnd;
                   },
                   failure: function ( result, request ) {
                        $.notify({message:'An Error Occurred.'},{type:'warning',delay:2000,offset:10});
                   }
                });
                */
            }else{
                gs.clearFilter();
            }
    },
      }],
	},//END form1
	'form2': [{
          text: 'BioDaliance (Window)',
          scope: this,
          handler: function(){drawBioDalModal();},
      },{
          text: 'BioDaliance (Panel)',
          scope: this,
          handler: function(){drawBioDalPanel();},
              /*
      },{
          text: 'jBrowse (Window)',
          scope: this,
          handler: function(){drawJBrowseModal();},
      },{
          text: 'jBrowse (Panel)',
          scope: this,
          handler: function(){drawJBrowsePanel();},
          */
      }],
	//END form2
	'form3':{
		layout: {
			type: 'vbox',
			align:'stretch',
			pack: 'start'
		},
		//flex: 2,
		border: false,
		items: [
			Ext.create('Ext.tree.Panel', {
				id: prefix.figaro+'TreeFilter',
				title: 'Tree Filter',
				border: false,
				store: Ext.create('Ext.data.TreeStore', {
					id: prefix.figaro+'TreeStore',
					proxy:{
						type: 'ajax',
						url: prefix.source+'/getJSONFilter.php',
						reader: {
							type: 'json',
							root: 'children',
						},
					}
				}),
				listeners: {
					itemclick: function(s,r) {
						if(r.data.leaf){
							fItem = {property:r.parentNode.data.text,value:r.data.text,type:"text",exact:true,incExc:false,}
							var gs = Ext.getCmp(prefix.figaro+'TableGrid').getStore();
							gs.clearFilter(true);
							gs.filter(fItem);            
							//dump(r.data);
						}
					}
				},
				rootVisible: false, 
			}),
		],
      buttons: [{
          text: 'Reset',
          scope: this,
          handler: resetFilters,
      }]
	},//END form3
	'form4':[
					{
					id: 'BioDButtonsHolder',
					xtype: 'panel',
					//html: 'testing',
					layout: {
						type: 'hbox',
						align: 'stretch',
					},
					items: [
				{
				id: 'BioDalHolder',
				xtype: 'panel',
				html : '<div id=\'svgHolder\'>Dalliance goes here...</div>',
				flex : 3,
				},
				Ext.create('Ext.grid.Panel', {
						xtype: 'gridpanel',
						title: 'Selected Samples:',
						id: prefix.figaro+'SelectedGrid',
						store: Ext.create('Ext.data.Store',{
							fields : ['id','fname','dsource','ctype','style'],
							//data: bdw.selects,
							paging : false,
							autoLoad: true,
							listeners:{refresh:function(){(typeof bdw.sources=='undefined'||!getObj('svgHolder'))?0:loadBioD()}},
						}),
						columnLines: true,
						columns: [
							{text:'ID',name:'id',dataIndex:'id',type:'string',hidden:true},
							{text:'File Name',name:'fname',dataIndex:'fname',type:'string',editor: {
								xtype: 'textfield',
								allowBlank: false
							}},
							{text:'Data Source',name:'dsource',dataIndex:'dsource',type:'string'},
							{text:'Cell Type',name:'ctype',dataIndex:'ctype',type:'string'},
							{text:'Style',name:'style',dataIndex:'style',type:'string',
								editor: {xtype:'combo',
									store: new Ext.data.SimpleStore({
										id: 'figStyleCombo',
										fields: ['styleType'],
										data : [
											['dot plot'],
											['line plot'],
											['histogram'],
											['intervals/segmentation'],
											['Genecode']
										]
									}),
									displayField:'styleType',
									valueField: 'styleType',
									mode: 'local',
									typeAhead: false,
									triggerAction: 'all',
									lazyRender: true,
									emptyText: 'Select Style...'
								}
							},{
			text:'Remove',
			name:'Remove',
			dataIndex:'id',
			width:80,
			align: 'center',
			sortable:false,
			type:'string',
			useNull:false,
			renderer: function(value,meta,record,rowIndex){
				var outText='<i class="fa fa-times" aria-hidden="true" id="drop_'+rowIndex+
                                    '" onClick="qButtons(this.id,\''+value+
                                    '\');" data-qtip="Remove this row."></i>';
				return outText;
			}
		}
						],
						selModel: Ext.create('Ext.selection.CheckboxModel', {
							mode: 'SIMPLE',
							checkOnly: true
						}),
						forceFit:true,
						plugins: [
							Ext.create('Ext.grid.plugin.RowEditing', {
								clicksToEdit: 2
							})
						],
						listeners:{
							edit:function(f,e){
								var objIndex = bdw.sources.index[e.record.data.id];
								bdw.sources.data[objIndex].name = e.newValues.fname;
								bdw.sources.data[objIndex].style = bigVars.figaro.formatting[e.record.data.style].style;
								loadBioD();
							},
						},
						flex:2,
					}),
					],
					flex : 1,
					buttons: [
						{
							text: 'Hide BioDal',
							cls: 'btn btn-sm btn-primary',
							scope: this,
							handler: function(){
								collapsePane('figSelectedGrid','BioDalHolder');
							}
						},
						{
							text: 'Hide Cart',
							cls: 'btn btn-sm btn-primary',
							scope: this,
							handler: function(){
								collapsePane('BioDalHolder','figSelectedGrid');
							}
						},
						{
							text: 'Show Both',
							cls: 'btn btn-sm btn-info',
							scope: this,
							handler: function(){
								collapsePane('BioDalHolder','figSelectedGrid',1);
							}
						},
						{
							text: 'Reset Tracks',
							cls: 'btn btn-sm btn-warning',
							scope: this,
							handler: function(){
								initSources();	
								//loadBioD();
								Ext.getCmp('figSelectedGrid').store.loadData([]);
							}
						},
						{
						text: 'Close Panel',
						cls: 'btn btn-sm btn-danger',
						scope: this,
						handler: function(){
							if(getObj('figBioDalExportPanel')===null||getObj('figBioDalExportPanel').firstChild.firstChild.firstChild===null){
								Ext.getCmp(prefix.figaro+'BioDalExportModal').hide();
								cleanEle(prefix.figaro+'BioDalExportModal');
							}else{
								Ext.getCmp(prefix.figaro+'BioDalExportPanel').hide();
								cleanEle(prefix.figaro+'BioDalExportPanel');
							}
						}
					}],
				}
		],
	'form5':{
		layout: {
			type: 'vbox',
			align:'stretch',
			pack: 'start'
		},
		//flex: 2,
		border: false,
		items: [
			Ext.create('Ext.panel.Panel', {
				id: prefix.figaro+'holdertest',
				title: 'Set Region',
				html: '<div style="width:100%;padding:5px 10px;"><p>Change default Region:</p>'+
					'<p>Chromosome: <input id="region_chr" name="region_chr" type="text" value="" size="4"/></p>'+
					'<p>Start: <input id="region_viewStart" name="region_viewStart" type="text" value="" size="10"/></p>'+
					'<p>End: <input id="region_viewEnd" name="region_viewEnd" type="text" value="" size="10"/></p></div>',
				border: false,
				listeners: {
                                    afterrender: function(){
                                            getObj('region_chr').value=bigVars.figaro.bdregion.chr;
                                            getObj('region_viewStart').value=bigVars.figaro.bdregion.viewStart;
                                            getObj('region_viewEnd').value=bigVars.figaro.bdregion.viewEnd;
                                            /*
                                                Ext.Ajax.request({
                                                   url:prefix.source+'/getJSONSession.php',
                                                   params: {'init':1},
                                                   success: function(response){
                                                        var parsedResponse = JSON.parse(response.responseText);
                                                        var usrReg=parsedResponse.Init[0].Region;
                                                        var usrFltr=JSON.parse(parsedResponse.Init[0].Filters);
                                                        if(usrFltr!==null){
                                                            var gs = Ext.getCmp(prefix.figaro+'TableGrid').getStore();
                                                            gs.clearFilter(true);
                                                            gs.filter(usrFltr);            
                                                        }
                                                        $.notify({message: 'Successfully Loaded Session: '},{type:'success',delay:2000,offset:10});
                                                        bigVars.figaro.bdregion.chr=usrReg.substr(0,usrReg.search(':'));
                                                        bigVars.figaro.bdregion.viewStart=usrReg.substr(usrReg.search(':')+1).split('-')[0];
                                                        bigVars.figaro.bdregion.viewEnd=usrReg.substr(usrReg.search(':')+1).split('-')[1];
                                                        getObj('region_chr').value=bigVars.figaro.bdregion.chr;
                                                        getObj('region_viewStart').value=bigVars.figaro.bdregion.viewStart;
                                                        getObj('region_viewEnd').value=bigVars.figaro.bdregion.viewEnd;
                                                   },
                                                   failure: function ( result, request ) {
                                                        $.notify({message:'An Error Occurred.'},{type:'warning',delay:2000,offset:10});
                                                   }
                                                });
                                                */
                                    }
				}
			}),
		],
      	buttons: [{
        	text: 'Save',
			scope: this,
			handler: function(){
                            if(!isNumeric(getObj('region_viewStart').value)||!isNumeric(getObj('region_viewEnd').value)){
                                    $.notify({message:'Genomic region start/end must be numeric.'},{type:'warning',delay:3000,offset:10});
                                    return;
                            }
                            if(parseInt(getObj('region_viewStart').value,10)<0||parseInt(getObj('region_viewEnd').value,10)<0){
                                    $.notify({message:'Genomic region start/end must be higher than zero.'},{type:'warning',delay:3000,offset:10});
                                    return;
                            }
                            if(parseInt(getObj('region_viewStart').value,10)>parseInt(getObj('region_viewEnd').value,10)){
                                    $.notify({message:'Genomic region endpoint cannot be defined as lower than start point.'},{type:'warning',delay:3000,offset:10});
                                    return;
                            }
                            bigVars.figaro.bdregion.chr=getObj('region_chr').value;
                            bigVars.figaro.bdregion.viewStart=getObj('region_viewStart').value;
                            bigVars.figaro.bdregion.viewEnd=getObj('region_viewEnd').value;
                            if(typeof bdw.brwsrObj!=='undefined'){
                                bdw.brwsrObj.chr=bigVars.figaro.bdregion.chr;
                                bdw.brwsrObj.viewStart=parseInt(bigVars.figaro.bdregion.viewStart,10);
                                bdw.brwsrObj.viewEnd=parseInt(bigVars.figaro.bdregion.viewEnd,10);
                                bdw.brwsrObj.refresh();
                            }
                            /*
                            Ext.Ajax.request({
                               url:prefix.source+'/getJSONSession.php',
                               params: {'region':bigVars.figaro.bdregion.chr+':'+bigVars.figaro.bdregion.viewStart+'-'+bigVars.figaro.bdregion.viewEnd,'action':'set'},
                               success: function(response){
                                    var parsedResponse = JSON.parse(response.responseText);
                                    $.notify({message:parsedResponse.msg},{type:'success',delay:2000,offset:10});
                               },
                               failure: function ( result, request ) {
                                    $.notify({message:'An Error Occurred: '+parsedResponse.msg},{type:'danger',delay:5000,offset:10});
                               }
                            });
                            */
			}
      }]
	},
	'form6':[
		{
			id: 'jBrowseButtonsHolder',
			xtype: 'panel',
			//html: 'testing',
			layout: {
				type: 'hbox',
				align: 'stretch',
			},
			items: [
				{
				id: 'JBrowseHolder',
				xtype: 'panel',
				html : '<div id=\'jbHolder\'>jbrowse goes here...</div>',
				flex : 1,
				},
			],
		flex : 1,
		buttons: [
			{
			text: 'Close Panel',
			cls: 'btn btn-sm btn-danger',
			scope: this,
			handler: function(){
				if(getObj(prefix.figaro+'JBrowseExportPanel')===null||getObj(prefix.figaro+'JBrowseExportPanel').firstChild.firstChild.firstChild===null){
					Ext.getCmp(prefix.figaro+'JBrowseExportModal').hide();
					cleanEle(prefix.figaro+'JBrowseExportModal');
				}else{
					Ext.getCmp(prefix.figaro+'JBrowseExportPanel').hide();
					cleanEle(prefix.figaro+'JBrowseExportPanel');
				}
			}
			}
		],
		}
		]
}};

Ext.create('Ext.data.JsonStore', {
	storeId: prefix.figaro+'Store',
	proxy: {
		type: 'ajax',
		url: prefix.source+'/getJSONSources.php',
		reader: {
			type: 'json',
			root: 'data',
			totalProperty: 'totalCount'
		},
        enablePaging: true,
		sortRoot:'Identifier',
		encodeFilters: function(filters) {
            var min = [],
                length = filters.length,
                i = 0;
            for (; i < length; i++) {
                min[i] = {
                    property: filters[i].property,
                    value: filters[i].value,
		    type: filters[i].type,
                    exact: filters[i].exact,
                    exclude: filters[i].incExc,
                    externalFile: filters[i].eFile,
                    externalField: filters[i].eField,
                };
            }
            return this.applyEncoding(min);
        }
	},
	autoDestroy: true,
	remoteSort: true,
	autoLoad: false,
	pageSize: 50,
	remotePaging: true,
	remoteFilter: true,
	fields: getfilesColumns(),
	initialTotalCount:0,
});

/*
showFiltersNEW=function(){
    eleID = Ext.getCmp('addFilter').getValue();
    var v = Ext.getCmp('addFilter').getValue();
    var record = Ext.getCmp('addFilter').findRecord(Ext.getCmp('addFilter').valueField 
		|| Ext.getCmp('addFilter').displayField, v);
    var index = Ext.getCmp('addFilter').store.indexOf(record);
    Ext.getCmp('dFilters').add(fConfigs[index]);
    Ext.getCmp('addFilter').store.remove(record);
    Ext.getCmp('addFilter').select(null);
}
*/

//hiddenForm.getForm().submit();

Ext.define(prefix.figaro+'Tab', {
	extend: 'Ext.panel.Panel',
	id: prefix.figaro+'TabPanel',
	title: 'Figaro', 
    autoScroll:true,
    scroll:true,
    layout: {
     	type: 'hbox',
     	align:'stretch',
     	pack: 'start'
    },
    items:[
        {
            id: prefix.figaro+'Filter',
            region: 'west',
            flex:1,
            collapsible: false,
            autoScroll:true,
            items:[
                bigVars.figaro.form1,
                bigVars.figaro.form3,
                bigVars.figaro.form5,
            ],
        },{xtype: 'splitter'},{
            xtype: 'container',
            id: prefix.figaro+'GridFrame',
                            flex: 6,
            layout: {
                    type: 'vbox',
                    align:'stretch',
                    pack: 'start'
            },
            autoScroll:true,
            style: 'padding:10px;background:#f5f5f5',
                    items:[
                        {
                            id: prefix.figaro+'loadsavebar',
                            scrollable: true,
                            layout     : {
                                    type: 'hbox',
                                    pack: 'start',
                                    align: 'stretch',
                            },
                            border: 'none',
                            items:[
                                      /*{
                                    xtype: 'toolbar',
                                    dock: 'top',
                                    border: 'none',
                                    items: [{
                                            text: 'Load Set/Filter',
                                            id: prefix.figaro+'LoadSaves',
                                            menu:[],
                                            listeners:{
                                                    afterrender:function(){makeLoadMenu('figLoadSaves');}
                                            }
                                    }]
                                },{
                                     xtype:'button',
                                     text: 'Load Selected',
                                     margin:5,
                                     scope: this,
                                     handler: function(){
                                        (Ext.getCmp('figTableGrid').store.filters.items.length>0)?Ext.getCmp('figTableGrid').store.clearFilter():{};
                                         var selectedObj = [];
                                         Ext.getCmp('figLoadSaves').menu.items.items.forEach(function(rec){
                                             (rec.checked==true)?selectedObj.push(rec.value):{};
                                         });
                                         Ext.getCmp('figTableGrid').store.load({
                                             params:{fsids:selectedObj.join(',')},
                                             callback:function(){
            Ext.Ajax.request({
               url:prefix.source+'/getJSONSaves.php',
               params: {fsids:selectedObj.join(',')},
               success: function(response){
                    var parsedResponse = JSON.parse(response.responseText);
                    for(f=0;f<parsedResponse.filters.length;f++){
                        var fpObj=JSON.parse(parsedResponse.filters[f].Filter_Params);
                        Ext.getCmp('figTableGrid').store.filter(fpObj,true);
                    }
               },
               failure: function ( result, request ) {
                    $.notify({message:'An Error Occurred.'},{type:'warning',delay:2000,offset:10});
               }
            });
                                             },
                                         });
                                     }
                                },*/{
                                            xtype:'button',
                                      text: 'Export Set',
                                            margin:5,
                                      scope: this,
                                      handler: function(){
    if(Ext.getCmp('figSaveOptions')){
    Ext.getCmp('figSaveOptions').show();
    }else{
    Ext.create('Ext.window.Window', {
    id      : prefix.figaro+'SaveOptions',
    modal      : true,
    centered   : true,
    scrollable : true,
    title: 'Save Filter/Set',
    layout     : {
            type  : 'hbox',
            pack  : 'start',
            align : 'stretch'
    },
    items      : [{
            xtype: 'form',id:'saveTypeForm',items:[
                    {
                    xtype: 'fieldset',
                    flex: 1,
                    title: 'Save Settings',
                    defaultType: 'radio', // each item will be a checkbox
                    layout: 'anchor',
                    border: 'none',
                    style: 'margin: 10px 0 30px 0',
                    defaults: {
                            anchor: '100%',
                            hideEmptyLabel: false
                    },
                    items: [{
                            xtype: 'textfield',
                            name: 'save_name',
                            fieldLabel: 'Save Name: ',
                            allowBlank: false,  // requires a non-empty value
                            margin:5,
                            border: 'none',
                    },{
                            xtype: 'textfield',
                            name: 'save_desc',
                            fieldLabel: 'Save Description: ',
                            allowBlank: false,  // requires a non-empty value
                            margin:5,
                            border: 'none',
                    },{
                            fieldLabel: 'Save Type: ',
                            boxLabel: 'Filters',
                            name: 'save_type',
                            inputValue: 'filter',
                            margin:5,
                            border: 'none',
                    },{
                            checked: true,
                            boxLabel: 'Sample Set',
                            name: 'save_type',
                            inputValue: 'set',
                            margin:5,
                            border: 'none',
                    },{
                            xtype: 'button',
                            text: 'Save',
                            handler: function(){
                                var paramObj = Ext.getCmp('saveTypeForm').getValues();
                                if(paramObj.save_type=='set'){
                                    var fids = [];
                                    var tObj = {};
                                    Ext.getCmp('figTableGrid').store.data.items.forEach(function(rec){
                                        fids.push(rec.raw.FID);
                                        if(typeof tObj.src !== 'undefined'){
                                            if(tObj.src.filter(function(sauce){return sauce.url == rec.raw['Source URL'];}).length>0){
                                                tObj.src[tObj.src.findIndex(x => x.url==rec.raw['Source URL'])].fids.push(rec.raw['FID']);
                                            }else{
                                                tObj.src.push({url:rec.raw['Source URL'],fids:[rec.raw.FID]});
                                            }
                                        }else{
                                            tObj.src=[{url:rec.raw['Source URL'],fids:[rec.raw.FID]}];
                                        }
                                    });
                                    paramObj.src = tObj.src;
                                    uriContent = "data:application/octet-stream," + encodeURIComponent(JSON.stringify(paramObj));
                                    newWindow = window.open(uriContent, 'JSON');
                                }else{
                                    paramObj.fltrJSON=JSON.stringify(Ext.getCmp('figTableGrid').getStore().filters.items);
                                }
                            },
                    }]
            }]
    }],
    listeners: {
            beforerender: function(){
                    if(Ext.getCmp(prefix.figaro+'BioDalExportPanel')){

                    }
            },
            afterrender: function(){
                    (typeof rowID==='undefined')?getSelectsForBioD():getSelectsForBioD(rowID);
            }
    }
    }).show();
    }
                                            },
                                    //{Ext.getCmp('figTableGrid').getStore().filters.items.forEach(function(rec){dump(rec);});}
                              },{text:'|',margin:5},
                                {
                              //{xtype:'textfield', fieldLabel:'Data Source', id:'sourceURL', name:'sourceURL', value:prefix.source},
                                /*
                                     xtype:'button',
                                     text: 'Load Data Source',
                                     margin:5,
                                     scope: this,
                                     handler: function(){
                                         prefix.source=Ext.getCmp('sourceURL').value;
                                         Ext.getCmp(prefix.figaro+'TableGrid').store.proxy.url=prefix.source+'/getJSONQuery.php';
                                         Ext.getCmp(prefix.figaro+'TableGrid').store.removeAll();
                                         Ext.getCmp(prefix.figaro+'TableGrid').store.load();
                                     },
                            },{
                            */
                                            xtype:'button',
                                            text: 'Sources',
                                            margin:5,
                                            id: prefix.figaro+'SourceMenu',
                                            menu:[
                                                    {xtype:'container',layout:'hbox',items:[
                                                        {xtype:'textfield',id:'customURL',width:100},
                                                        {xtype:'button',text:'Add',handler:function(){
                                                                var custURL = Ext.getCmp('customURL').value;
                                                                var menuObj = Ext.getCmp(prefix.figaro+'SourceMenu').menu;
                                                                menuObj.add({text:custURL,value:custURL,checked:true});
                                                                Ext.getCmp('customURL').setValue('');
                                                            }
                                                        }
                                                    ]},
                                                    {text:'NIAGADS GADB',value:'http://gadb.niagads.org',checked:true},
                                                    {text:'LSWang Lab GADB',value:'http://gadb.lisanwanglab.org',checked:false}
                                                ],
                                            listeners:{
                                                    afterrender:pullSources
                                            }
                            },{
                                     xtype:'button',
                                     text: 'Reload Sources',
                                     margin:5,
                                     scope: this,
                                     handler: pullSources
                            }]
                        },
                                    Ext.create('Ext.grid.Panel', {
                                            xtype: 'gridpanel',
                                            title: 'Please select the samples you wish to view:',
                                            id: prefix.figaro+'TableGrid',
                                            //height: 590,
                                            flex:1,
                                            store: Ext.data.StoreManager.lookup(prefix.figaro+'Store'),
                                            columnLines: true,
                                            columns: getfilesColumns(),
                                            selModel: Ext.create('Ext.selection.CheckboxModel', {
                                                    mode: 'SIMPLE',
                                                    checkOnly: true
                                            }),
                                            viewConfig:{ 
                                                    stripeRows: false, 
                                                    getRowClass: function(record) { 
                                                            return (record.get('File format').search('bigBed')!=-1 ||
                                                                    record.get('File format').search('bigWig')!=-1 ||
                                                                    record.get('File format').search('bed')!=-1 
                                                                    )? 'good-row' : 'bad-row'; 
                                                    } 
                                            },
                                            dockedItems: [{
                                                                    xtype: 'toolbar',
                                                                    dock: 'bottom',
                                                                    ui: 'footer',
                                                                    layout: {
                                                                            pack: 'center'
                                                                    },
                                                    items: bigVars.figaro.form2,
                                            }],
                                            bbar: Ext.create('Ext.PagingToolbar', {
                                                            store: Ext.data.StoreManager.lookup(prefix.figaro+'Store'),
                                            }),
                                    })
                            ],//end items
			}
	],
    initComponent: function(){
if (window.FileReader) {
  function dragEvent (ev) {
    ev.stopPropagation (); 
    ev.preventDefault ();
    if (ev.type == 'drop') {
      var rows = [];
      var reader = new FileReader ();
      reader.onloadend = function (ev) {
	//alert(this.result);
	heh = this.result.split('\n');
	for(i=0;i<heh.length;i++){
            if(heh[i]!=''){
                rows.push(heh[i].replace(/(\r\n|\n|\r)/gm,''));
            }
	}
	Ext.Msg.confirm('Import JSON FIDs Into Grid?', 'Are you sure?', function (id, value) {
            if (id === 'yes') {
                var paramObj = {};
                var parsey = JSON.parse(this.result)
                for (var key in parsey) {
                    if (parsey.hasOwnProperty(key)) {
                        if(parsey[key].constructor === Array){
                            paramObj[key] = JSON.stringify(parsey[key]);
                        }else{
                            paramObj[key] = parsey[key];
                        }
                    }
                }
                paramObj.action = 'import';
                if(waitForDelay()){
                    Ext.getCmp(prefix.figaro+'TableGrid').store.proxy.extraParams=paramObj;
                    Ext.getCmp(prefix.figaro+'TableGrid').store.load();
                }
                /*
                Ext.Ajax.request({
                   url:prefix.source+'/getJSONSources.php',
                   params: paramObj,
                   success: function(response){
                        var parsedResponse = JSON.parse(response.responseText);
                        //dump(parsedResponse.success);
                        if(parsedResponse.data.length>0){
                                $.notify({message: 'Loaded FIDs Successfully: '+parsedResponse.msg},{type:'success',delay:2000,offset:10});
                                Ext.getCmp(prefix.figaro+'TableGrid').store.removeAll();
                                Ext.getCmp(prefix.figaro+'TableGrid').store.loadData(parsedResponse.data);
                        }else{
                                $.notify({message: 'Promlem Loading FIDs: '+parsedResponse.msg},{type:'warning',delay:3000,offset:10});
                        }
                   },
                   failure: function ( result, request ) {
                        $.notify({message:'An Error Occurred.'},{type:'warning',delay:2000,offset:10});
                   }
                });
                /*
		Ext.Ajax.request({
			url: 'importCart.php',
			params: { 
				action:'import',
				sampleIDs:rows.join(','),
            			userID: user_info.id
			 },
			success: function(response){
				Ext.example.msg('Import Response:',response.responseText,3000); 
				titleCartUpdate();
			},
			failure: function(){
				Ext.example.msg('Import Response:','Action failed.',3000); 
			}
		});
                */
	}
	}, this);
	
	};
      reader.readAsText (ev.dataTransfer.files[0]);
    }  
  }

  document.addEventListener ('dragenter', dragEvent, false);
  document.addEventListener ('dragover', dragEvent, false);
  document.addEventListener ('drop', dragEvent, false);
}
		//bigVars.figaro.form1.items[0].items[0].items[0].menu.forEach(function(rec){Ext.getCmp(prefix.figaro+'TreeFilter').store.tree.root.childNodes[0].appendChild({ text:rec.text, leaf: true})});
		this.callParent();
	},
});

Ext.onReady(function(){
	initSources();
});

