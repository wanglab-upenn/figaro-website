var getfilesColumns = function(){
	var columns=[
		{
			text:'Tools',
			name:'Tools',
			dataIndex:'Identifier',
			width:80,
			align: 'center',
			sortable:false,
			type:'string',
			useNull:false,
			renderer: function(value,meta,record,rowIndex){
				var outText=''+
					'<i class="fa fa-plus-square" aria-hidden="true" id="add_'+rowIndex+'" onClick="qButtons(this.id,\''+value+'\')" data-qtip="Add this row to the tracks table."></i>'+
					'&nbsp;'+
					'&nbsp;'+
					'<i class="fa fa-cart-plus" aria-hidden="true" id="pan_'+rowIndex+'" onClick="qButtons(this.id)" data-qtip="Open this row in its own Panel."></i>'+
					'&nbsp;'+
					'&nbsp;'+
					'<i class="fa fa-location-arrow" aria-hidden="true" id="pop_'+rowIndex+'" onClick="qButtons(this.id)" data-qtip="Open this row in its own Window."></i>';
				return outText;
			}
		},{
			text:'Identifier',
			name:'Identifier',
			dataIndex:'Identifier',
			width:130,
			sortable:true,
			type:'string',
			useNull:false
		},{
			text:'Source URL',
			name:'Source URL',
			dataIndex:'Source URL',
			width:120,
			sortable:false,
			type:'string',
			useNull:false
		},{
			text:'Data Source',
			name:'Data Source',
			dataIndex:'Data Source',
			width:120,
			sortable:true,
			type:'string',
			useNull:false
		},{
			text:'File name',
			name:'File name',
			dataIndex:'File name',
			width:250,
			sortable:true,
			type:'string',
			useNull:false
		},{
			text:'Cell type',
			name:'Cell type',
			dataIndex:'Cell type',
			width:130,
			sortable:true,
			type:'string',
			useNull:false
		},{
			text:'Antibody',
			name:'Antibody',
			dataIndex:'Antibody',
			width:130,
			sortable:true,
			type:'string',
			useNull:false
		},{
			text:'Assay',
			name:'Assay',
			dataIndex:'Assay',
			width:105,
			sortable:true,
			type:'string',
			useNull:false
		},{
			text:'File format',
			name:'File format',
			dataIndex:'File format',
			width:120,
			sortable:true,
			type:'string',
			useNull:false
		},{
			text:'File size',
			name:'File size',
			dataIndex:'File size',
			width:100,
			sortable:true,
			type:'string',
			useNull:false
		},{
			text:'File path',
			name:'File path',
			dataIndex:'File path',
			width:300,
			sortable:true,
			type:'string',
			useNull:false
		},{
			text:'Genome Build',
			name:'Genome build',
			dataIndex:'Genome build',
			width:100,
			sortable:true,
			type:'string',
			useNull:false
		},{
			text:'Downloaded Date',
			name:'Downloaded date',
			dataIndex:'Downloaded date',
			width:120,
			sortable:true,
			type:'string',
			useNull:false
		},{
			text:'Release Date',
			name:'Release date',
			dataIndex:'Release date',
			width:100,
			sortable:true,
			type:'string',
			useNull:false
		},{
			text:'Date Added',
			name:'Date added',
			dataIndex:'Date added',
			width:100,
			sortable:true,
			type:'string',
			useNull:false
		},{
			text:'Link out URL',
			name:'Link out URL',
			dataIndex:'Link out URL',
			width:300,
			sortable:true,
			type:'string',
			useNull:false
		},{
			text:'Download URL',
			name:'Download URL',
			dataIndex:'Download URL',
			width:300,
			sortable:true,
			type:'string',
			useNull:false
		},{
			text:'md5',
			name:'md5',
			dataIndex:'md5',
			width:280,
			sortable:true,
			type:'string',
			useNull:false
		},
	];
	return columns;
};
