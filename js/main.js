Ext.create('Ext.data.JsonStore', {
	storeId:'GADBStore',
	pageSize: 100,
	proxy: {
		type: 'ajax',
		url: 'getJSONQuery.php',
		reader: {
			type: 'json',
			root: 'data',
			totalProperty: 'totalCount'
		},
        enablePaging: true,
		sortRoot:'Identifier',
		encodeFilters: function(filters) {
            var min = [],
                length = filters.length,
                i = 0;
            for (; i < length; i++) {
                min[i] = {
                    property: filters[i].property,
                    value: filters[i].value,
					type: filters[i].sType,
                    exact: filters[i].exact,
                    exclude: filters[i].incExc,
                };
            }
            return this.applyEncoding(min);
        }
	},
	autoDestroy: true,
	remoteSort: true,
	autoLoad: true,
	pageSize: 100,
	remotePaging: true,
	remoteFilter: true,
	fields: getfilesColumns(),
	initialTotalCount:0,
});

var filterIds = [];
var fChkItems=['fltDataSource','fltAssay','fltDataType',];
var fRangeBoxes=[];
//var fRangeBoxes=['fltTestMinMax'];
var searchFields = ['fltAntibody','fltCellType'];
var fConfigs = [];

var bdw = "";

/*
showFiltersNEW=function(){
    eleID = Ext.getCmp('addFilter').getValue();
    var v = Ext.getCmp('addFilter').getValue();
    var record = Ext.getCmp('addFilter').findRecord(Ext.getCmp('addFilter').valueField 
		|| Ext.getCmp('addFilter').displayField, v);
    var index = Ext.getCmp('addFilter').store.indexOf(record);
    Ext.getCmp('dFilters').add(fConfigs[index]);
    Ext.getCmp('addFilter').store.remove(record);
    Ext.getCmp('addFilter').select(null);
}
*/

resetFilters=function(){
    chkItems = fChkItems;
    Ext.getCmp('dFilters').removeAll();
    //Ext.getCmp('addFilter').store.reload();
    Ext.getCmp('tableImages').getStore().clearFilter();
};

applyFilters=function(){
    var exclusions=[];
    
    chkItems = fChkItems;
    cbItems  = filterIds;
    rBoxes   = fRangeBoxes;
	srchflds = searchFields;
    
    for(m=0;m<searchFields.length;++m){
	if(Ext.getCmp(searchFields[m])){
	    var myObj=Ext.getCmp(searchFields[m]);
		console.log(myObj);
	    for (n=0;n< myObj.items.length;++n){
            	var itm=myObj.items.items[n];
              	if(!itm.empty){
                    exclusions.push({property:itm.mysqlField,value:itm.value,sType:'text',exact:myObj.items.items[myObj.items.indexOfKey(myObj.id+'Exact')].checked,incExc:myObj.items.items[myObj.items.indexOfKey(myObj.id+'Exclude')].checked});
                }
      	    }
	}
    }
    
    for(m=0;m<chkItems.length;++m){
	if(Ext.getCmp(chkItems[m])){
	    var myObj=Ext.getCmp(chkItems[m]);
	    for (n=0;n< myObj.menu.items.length;++n){
            	var itm=myObj.menu.items.items[n];
              	if(!itm.checked){
                    exclusions.push({property:itm.mysqlField, value:itm.mysqlValue});
                }
      	    }
	}
    }
    
    for(m=0;m<cbItems.length;++m){
      var myObj=Ext.getCmp(cbItems[m]);
      if (myObj.value != null){
        for (n=0;n< myObj.store.data.items.length;++n){
          var val=myObj.store.data.items[n].raw[0];
          if(val != myObj.value ){
            exclusions.push({property:myObj.mysqlField, value:val});
          }
        }
      }
    }
    
    for(m=0;m<rBoxes.length;++m){
      var myObj=Ext.getCmp(rBoxes[m]);
      var valueList=new Object();
      valueList.count=0;
      
      for (n=0;n<myObj.items.length;n++){
        var fld = myObj.items.items[n];
        if (fld.value != null){
          if (fld.fieldLabel=='Min'){
            valueList.Min = fld.value;
            valueList.count++;
          }
          if (fld.fieldLabel=='Max'){
            valueList.Max = fld.value;
            valueList.count++;
          }
        }
      }
      if (valueList.count>0){
         exclusions.push({property:myObj.mysqlField, value:valueList});
      }
    }
 
    var gs = Ext.getCmp('tableImages').getStore();
    
    if (exclusions.length>0){
		console.log(exclusions);
      	gs.clearFilter(true);
      	gs.filter(exclusions);            
    }else{
      	gs.clearFilter();
    }
};

//STATIC FILTERS
var filterList = [
	{
	xtype: 'toolbar',
	dock: 'top',
	border: 'none',
	items: [{
    	text: 'Data Source',
    	id:'fltDataSource',
    	menu: [
	//4DGenome,Braineac,CADD,dbSNP,DGV,Encode,FANTOM5,PhastCons,RegulomeDB,RoadMap,Vistaenhancers,smRNA
        	{text: '4DGenome',checked:true, mysqlField:'Data Source',mysqlValue:'4DGenome'},
        	{text: 'Braineac',checked:true, mysqlField:'Data Source',mysqlValue:'Braineac'},
        	{text: 'CADD',checked:true, mysqlField:'Data Source',mysqlValue:'CADD'},
        	{text: 'dbSNP',checked:true, mysqlField:'Data Source',mysqlValue:'dbSNP'},
        	{text: 'DGV',checked:true, mysqlField:'Data Source',mysqlValue:'DGV'},
        	{text: 'Encode',checked:true, mysqlField:'Data Source',mysqlValue:'Encode'},
        	{text: 'FANTOM5',checked:true, mysqlField:'Data Source',mysqlValue:'FANTOM5'},
        	{text: 'PhastCons',checked:true, mysqlField:'Data Source',mysqlValue:'PhastCons'},
        	{text: 'RegulomeDB',checked:true, mysqlField:'Data Source',mysqlValue:'RegulomeDB'},
        	{text: 'RoadMap',checked:true, mysqlField:'Data Source',mysqlValue:'RoadMap'},
        	{text: 'Vistaenhancers',checked:true, mysqlField:'Data Source',mysqlValue:'Vistaenhancers'},
        	{text: 'smRNA',checked:true, mysqlField:'Data Source',mysqlValue:'smRNA'},
    	]
	}]
  	},{
    xtype: 'toolbar',
    dock: 'top',
	border: 'none',
    items: [{
        text: 'Assay',
        id:'fltAssay',
        menu: [
			//NA,Gene expression (Affymetrix Exon 1.0 ST Arrays),DnaseSeq,ChipSeq
            {text: 'Gene expression (Affymetrix Exon 1.0 ST Arrays)',checked: true, mysqlField:'Assay',mysqlValue:'Gene expression (Affymetrix Exon 1.0 ST Arrays)'},
            {text: 'DnaseSeq',checked: true, mysqlField:'Assay',mysqlValue:'DnaseSeq'},
            {text: 'ChipSeq',checked: true, mysqlField:'Assay',mysqlValue:'ChipSeq'},
            {text: 'N/A',checked: true, mysqlField:'Assay',mysqlValue:'NA'},
        ]
    }]
    },{
    xtype: 'toolbar',
    dock: 'top',
	border: 'none',
    items: [{
        text: 'Data Type',
        id:'fltDataType',
        menu: [
			//NA,tss-enhancer associations,Differentially expressed,Expressed,Permissive enhancers,Robust enhancers,Ubiquitous enhancers
            {text:'tss-enhancer associations',checked:true, mysqlField:'Data type',mysqlValue:'tss-enhancer associations'},
            {text:'Differentially expressed',checked:true, mysqlField:'Data type',mysqlValue:'Differentially expressed'},
            {text:'Expressed',checked:true, mysqlField:'Data type',mysqlValue:'Expressed'},
            {text:'Permissive enhancers',checked:true, mysqlField:'Data type',mysqlValue:'Permissive enhancers'},
            {text:'Robust enhancers',checked:true, mysqlField:'Data type',mysqlValue:'Robust enhancers'},
            {text:'Ubiquitous enhancers',checked:true, mysqlField:'Data type',mysqlValue:'Ubiquitous enhancers'},
            {text:'N/A',checked:true, mysqlField:'Data type',mysqlValue:'NA'},
        ]
    }]
  	},{
		xtype: 'fieldset',
		id:'fltAntibody',
		title: 'Antibody',
		//hidden: true,
		collapsible: true,
		collapsed: true,
		defaults:{
			xtype:'textfield',
			//minValue: 0,
			//maxValue: 200,
			//maxLength:3,
			//enforceMaxLength:3,
			//hideTrigger: true,
			//keyNavEnabled: false,
			//mouseWheelEnabled: false,
		},
		items:[
			{mysqlField: 'Antibody',emptyText:'Search'},
            {id: 'fltAntibodyExact',name: 'fltAntibodyExact',xtype:'checkbox',boxLabel:'Exact',checked:false,},
			{id: 'fltAntibodyExclude',name: 'fltAntibodyExclude',xtype:'checkbox',boxLabel:'Exclude',checked:false,},
		],
  	},{
		xtype: 'fieldset',
		id:'fltCellType',
		title: 'Cell Type',
		//hidden: true,
		collapsible: true,
		collapsed: true,
		defaults:{
			xtype:'textfield',
			//minValue: 0,
			//maxValue: 200,
			//maxLength:3,
			//enforceMaxLength:3,
			//hideTrigger: true,
			//keyNavEnabled: false,
			//mouseWheelEnabled: false,
		},
		items:[
			{mysqlField: 'Cell Type',emptyText:'Search'},
            {id: 'fltCellTypeExact',name: 'fltCellTypeExact',xtype:'checkbox',boxLabel:'Exact',checked:false,},
            {id: 'fltCellTypeExclude',name: 'fltCellTypeExclude',xtype:'checkbox',boxLabel:'Exclude',checked:false,},
		],
/*
  	},{
		xtype: 'fieldset',
		id:'fltTestMinMax',
		title: 'MinMax',
		//hidden: true,
		collapsible: true,
		collapsed: true,
		mysqlField:'TEST NUMBERS',
		defaults:{
			xtype:'numberfield',
			minValue: 0,
			//maxValue: 200,
			//maxLength:3,
			//enforceMaxLength:3,
			hideTrigger: true,
			keyNavEnabled: false,
			mouseWheelEnabled: false,
			vtype:'numberrange',
		},
		items:[
			{id:'Min',fieldLabel:'Min',emptyText:'Min'},
			{id:'Max',fieldLabel:'Max',emptyText:'Max'},
		],
*/
	}	
	];

var form1 = {
      layout: 'vbox',
      items: [{
            xtype: 'fieldcontainer',
            id: 'bigtest',
            layout: {
                type: 'vbox',
                //align: 'stretch'
            },
            //url:'save-form.php',
            padding: '4 0 0 1',
            border: false,
            //cls: 'absolute-form-panel-body',
            defaultType: 'textfield',
            defaults: {
                width: 170
            },
            fieldDefaults: {
                labelAlign: 'top',
                //labelWidth: 100,
                labelStyle: 'font-weight:bold',
                border:false,
            },
            items: filterList,
      },{
            xtype: 'fieldcontainer',
            id: 'dFilters',
            layout: {
                type: 'vbox',
            },
            padding: '4 0 0 1',
            border: false,
            defaultType: 'textfield',
            defaults: {
                width: 170
            },
            fieldDefaults: {
                labelAlign: 'top',
                labelStyle: 'font-weight:bold',
                border:false,
            },
            //items: filterList,
      },{
            xtype: 'fieldcontainer',
            id: 'addFilterBox',
            layout: {
                type: 'vbox',
            },
            padding: '4 0 0 1',
            border: false,
            defaultType: 'textfield',
            defaults: {
                width: 170
            },
            fieldDefaults: {
                labelAlign: 'top',
                labelStyle: 'font-weight:bold',
                border:false,
            },
            //items: filterList,
      }],
      buttons: [{
          text: 'Reset',
          width:90,
          scope: this,
          handler: resetFilters,
      }, {
          text: 'Apply Filters',
          //width: 150,
          scope: this,
          handler: applyFilters,
      }],
};

var form2 = {
      layout: 'vbox',
      buttons: [/*{
          text: 'Export to BioDalliance',
          //width: 150,
          scope: this,
          handler: function(){
			Ext.create('Ext.window.Window', {
			id      : 'testmodal',
			modal      : true,
			centered   : true,
			scrollable : true,
			width      : '80%',
			height     : '80%',
			layout     : {
				type  : 'hbox',
				pack  : 'start',
				align : 'stretch'
			},
			items      : [
				{
				xtype: 'panel',
				html : '<div id=\'svgHolder\'>Dalliance goes here...</div>',
				style : 'padding: 10px 0 10px 10px',
				flex : 4,
				},
				{
				xtype: 'panel',
				id : 'modalRightPanel',
				html : 'Testing Functionality',
				style : 'padding: 10px',
				flex : 1,
				buttons: [{
					text: 'Load BioD',
					scope: this,
					handler: function(){
						loadBioD();
					}
				},{
					text: 'Add Source',
					scope: this,
					handler: function(){
						getSelectsForBioD();
						//bdw.addTier({name: 'Pseudogenes',desc: 'Consensus pseudogenes predicted by the Yale and UCSC pipelines / GENCODE 19',bwgURI: 'http://bd.lisanwanglab.org/barry_files/test_html/gencode.v19.2wayconspseudos.bed.bb'});
					}
				},{
					text: 'Remove Source',
					scope: this,
					handler: function(){
						bdw.removeTier({name: 'Pseudogenes',desc: 'Consensus pseudogenes predicted by the Yale and UCSC pipelines / GENCODE 19',bwgURI: 'http://bd.lisanwanglab.org/barry_files/test_html/gencode.v19.2wayconspseudos.bed.bb'});
					}
				}],
				}
			],
			initComponent: function(){
				getSelectsForBioD();
				loadBioD();
    			this.callParent();
			},
			}).show();
	
			},
      }*/],
};

//hiddenForm.getForm().submit();
var filesPanel = Ext.create('Ext.grid.Panel', {
	xtype: 'gridpanel',
	title: 'Please select the images you wish to view:',
	id:'tableImages',
	store: Ext.data.StoreManager.lookup('GADBStore'),
	columnLines: true,
	columns: getfilesColumns(),
	selModel: Ext.create('Ext.selection.CheckboxModel', {
    	mode: 'SIMPLE',
        checkOnly: true
    }),

	//selModel: {selType: 'checkboxmodel'},
   	height: 590,
	flex:1,
	//width: 900,
	dockedItems: [{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                layout: {
                    pack: 'center'
                },
		items: [
		{text: 'Download All',handler:function(){hiddenForm.getForm().submit();}},
		{text:'Export to Excel',handler: function(){
			var dPanel = Ext.getCmp('tableImages');
			var fullCount = dPanel.getStore().totalCount;
			dPanel.store.reload({
				params:{start:0,limit:fullCount},
				//callback executes upon async reload completion
				callback: function(records, operation, success) {  
				//console.debug("document.location:=" + document.location);
				document.location='data:application/vnd.ms-excel;base64,' +Base64.encode(dPanel.getExcelXml());
			}
			});
			}
		}
		]
	}],
	bbar: Ext.create('Ext.PagingToolbar', {
            store: Ext.data.StoreManager.lookup('GADBStore'),
	}),
});

Ext.define('TabGADB', {
	extend: 'Ext.panel.Panel',
    height: 750,
	id:'TabGADBPanel',
	title: 'GADB', 
    autoScroll:true,
    scroll:true,
    layout: {
     	type: 'hbox',
     	align:'stretch',
     	pack: 'start'
    },
    items:[
			{
          		id: 'app-options1',
          		title: 'Filter Builder',
          		region: 'west',
          		width: 200,
          		height:750,
          		collapsible: false,
          		autoScroll:true,
          		overflowX:'hidden',
          		overflowY:'hidden',
          		items:[form1,form2],
			},{
            	xtype: 'container',
            	id:'imagesRightSection',
				flex: 1,
            	layout: {
              		type: 'vbox',
              		align:'stretch',
              		pack: 'start'
            	},
            	autoScroll:true,
            	style: 'padding:10px;background:#f5f5f5',
        		items:[filesPanel],
			}
	],

    initComponent: function(){
/*
      Ext.Ajax.request({
        url: 'getJSONFiltersNEW.php',
        params: {},
        success: function(response){
	    var tOut1=[];
	//alert(response);
            var inData=[];
            var JSONdata = Ext.decode(response.responseText).data;
	//alert(JSONdata);
            Ext.each(JSONdata, function(op) {
                var aData=[];
                var menuData=[];
                var config="";
                var values=op.fValues.split(',');
                for(i=0;i<values.length;i++){
                    //aData.push([values[i],keys[i]]);
                    menuData.push({text:values[i],checked:true,mysqlField:op.fKey,mysqlValue:values[i]});
                }
                        fChkItems.push('flt'+op.fKey);
                        inData.push({value:'flt'+op.fKey,key:op.fKey});
                        
                        fConfigs.push({ xtype: 'toolbar',
                          dock: 'top',                          
                          items: [{text: op.fKey,
                                      id: 'flt'+op.fKey,
                                  hidden: false,
                                    menu: menuData,}],
                        });
	    });

            Ext.getCmp('addFilterBox').add({
                xtype: 'fieldset',
                title: 'Filters',
                label: 'test label',
                dock: 'top',
                    items: [{
                            xtype: 'combobox',
                            fieldLabel: 'Add Filter',
                            name: 'addFilter',
                            id:'addFilter',
                            valueField: 'value',
                            displayField: 'key',
                            store: new Ext.data.Store({fields: ['value','key'],data: inData,}),
                          },{
                            xtype: 'button',
                            text: 'Add Filter',
                            id:'addFilterButton',
                            handler: showFiltersNEW,
                          }]
            });
	},
     }); 
	*/

    	this.callParent();
    },
	
});

Ext.onReady(function(){
})

